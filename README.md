# QUESTION #

QUESTION is a python implementation of the analytical, laboratory and field test to be performed on any Shallow Water Equation Numerical Implementation. Explanation on the use and documentation of QUESTION can be found in the [project's wiki](https://bitbucket.org/sebastiandres/question/wiki/edit/Home).

## Requirements ##
- Python 2.7
- numpy >= 1.8
- scipy >= 0.14
- pyinstaller >= 2.1 (Only for creating the executables, not for running)

## Disclosure ##

Most of the current implemented tests have been taken from the article [1].
Specific references are given at execution and in the documentation for each specific test.
> [1] Synolakis, C.E., E.N. Bernard, V.V. Titov, U. Kânoğlu, and F.I. González(2007):
> Standards, criteria, and procedures for NOAA evaluation of tsunami numerical models.
> NOAA Tech. Memo. OAR PMEL-135, NOAA/Pacific Marine Environmental Laboratory, Seattle, WA, 55 pp. [[link]](http://nctr.pmel.noaa.gov/benchmark/SP_3053.pdf).

Some of the data produced by QUESTION is a transformation of the data
freely available at NOAA's benchmark section [[link]](http://nctr.pmel.noaa.gov/benchmark/).