import numpy as np
import pickle
import sys
import os

import SBFFormat
import LonLatHandler
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

'''
WARNING: Do not use this library directly. Use SBFFormat instead.
'''

################################################################################
# SAVE DATA IN SIMPLIFIED FORMAT
################################################################################
def save(to_path, sbf_dict):
    """
    Saves the sbf dictionary into sbf format on the provied folder.
    """
    print("Saving in simplified bathymetry format")
    # Make folder to save the data and define number format
    Common.mkdir(to_path)
    fmt = "%.8E"
    header = ""

    # Save the Bathymetry
    NB = sbf_dict["Mesh"].shape[0]
    B = np.zeros([NB,3])
    B[:,:2] = sbf_dict["Mesh"]
    B[:, 2] = sbf_dict["Bathymetry"]
    filename = os.path.join(to_path, "Bathymetry.txt")
    print("\tSaving Bathymetry points")
    np.savetxt(filename, B, fmt=fmt, header=header)

    # Save the Coordinate System
    filename = os.path.join(to_path, "CoordinateSystem.txt")
    print("\tSaving Coordinate System")

    # If CS is geophraphical, no need to pass the LinearizationPoint.
    if "LinearizationPoint" in sbf_dict:
        LonLatHandler.save_coordinate_system(filename, sbf_dict["CoordinateSystem"], sbf_dict["LinearizationPoint"])
    else:
        LonLatHandler.save_coordinate_system(filename, sbf_dict["CoordinateSystem"], None)

    # Save the Time Series Buoys
    if "TimeSeriesBuoyCoordinates" in sbf_dict:
        if len(sbf_dict["TimeSeriesBuoyCoordinates"].shape)==2:
            filename = os.path.join(to_path, "TimeSeriesBuoyCoordinates.txt")
            print("\tSaving Time Series Buoy Coordinates")
            np.savetxt(filename, sbf_dict["TimeSeriesBuoyCoordinates"], fmt=fmt, header=header)

    # Save the Time Series Buoys
    if "SpatialProfileBuoyCoordinates" in sbf_dict:
        # Check there's data to be saved
        if len(sbf_dict["SpatialProfileBuoyCoordinates"].shape)==2:
            filename = os.path.join(to_path, "SpatialProfileBuoyCoordinates.txt")
            print("\tSaving Spatial Profile Buoy Coordinates")
            np.savetxt(filename, sbf_dict["SpatialProfileBuoyCoordinates"], fmt=fmt, header=header)

    # Write README file with file format
    SBFFormat.write_readme(to_path)
    return
