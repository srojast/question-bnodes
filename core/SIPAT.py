import numpy as np
import csv
import sys
import os

import LonLatHandler
import RemoteFiles
import Default
import Common
import Okada

if Default.DEBUG_MODE:
    from IPython import embed

SCID_POSITION = 0

################################################################################
# MIF: Most important function
################################################################################
def generate_from_sipat_file(sipat_file, to_path, line_list, sid_list):
    """
    Creates the required folders in QUESTION format, using the sipat_file
    and selecting from line_list or sid_list
    """
    # Read the file
    list_reader = csv.reader(open(sipat_file,'r'))
    # Template to print for created scenarios
    template = "\nCreating scenario {0} for line {1} (sid {2}) from file {3}"
    # Store the created folders
    folders = []
    # Process selected files
    for line_number, params_list in enumerate(list_reader):
        # OBS: params_list is passed as a list of strings, not int or floats.
        sid = int(params_list[SCID_POSITION])
        if _scenario_was_selected(line_number, sid, line_list, sid_list):
            # From the csv line, create a dict for the values using implicit order
            sipat_mks_dic = _create_mks_dic(params_list)
            # Update the to_path to include the scenario id.
            target_path = to_path + "_sc{0}".format(sid)
            # Print some debugging info
            template_data = target_path, line_number, sid, os.path.basename(sipat_file)
            print(template.format(*template_data))
            # Create the scenario
            _create_scenario(target_path, sipat_mks_dic)
            # Store the created folder
            folders.append(target_path)

    return folders

################################################################################
# Creates the scenario from arguments
################################################################################
def _create_scenario(target_path, sipat_mks_dict):
    """
    Creates a SIPAT scenario.
    We store everything in mks, except Longitude and Latitude (in degrees).
    We always convert Longitude to range [-180, 180]
    """
    #print "Fix _create_scenario in SIPAT: Update the mesh to the final one"
    # Download the template (will be downloaded only once, then used from cache)
    RemoteFiles.download(target_path, "sipat_mesh")
    # Read some files
    NodeCoords = np.loadtxt( os.path.join(target_path, "Mesh", "NodeCoords.txt"), dtype=float )
    ElemNodes = np.loadtxt( os.path.join(target_path, "Mesh", "ElemNodes.txt"), dtype=int )
    m2d_fpath = os.path.join(target_path, "Mesh", "Optional", "Meter2Degrees.txt")
    if os.path.exists(m2d_fpath):
        Meter2Degrees = np.loadtxt( m2d_fpath )
        Meter2Degrees[:,2] = LonLatHandler.longitude_to_range(Meter2Degrees[:,2])
        Lon = Meter2Degrees[:,2]
        Lat = Meter2Degrees[:,3]
        print("Longitude in range [{0:+.1f}, {1:+.1f}] (degrees)".format(Lon.min(), Lon.max()))
        print("Latitude  in range [{0:+.1f}, {1:+.1f}] (degrees)".format(Lat.min(), Lat.max()))
    else:
        Meter2Degrees = None
    # dict of params
    elastic_parameters_dict = sipat_mks_dict["elastic_parameters"]
    print("Computing okada surface for sipat escenario:")
    print("\tId: {0}".format(sipat_mks_dict["general_info"]["scID"]))
    print("\tMagnitude: {0}".format(sipat_mks_dict["general_info"]["scMw"]))
    print("\tLongitude (degrees): {0}".format(sipat_mks_dict["general_info"]["scLon"]))
    print("\tLatitude (degrees): {0}".format(sipat_mks_dict["general_info"]["scLat"]))
    print("\tDepth (meters): {0}".format(sipat_mks_dict["general_info"]["scDepth"]))
    # Create the okada surface for the params.
    # We start with zero, so even if all false we have a flat surface
    NElems = ElemNodes.shape[0]
    subfaults_W = [ np.zeros(NElems), ]
    subfaults_HU = [ np.zeros(NElems), ]
    subfaults_HV = [ np.zeros(NElems), ]
    for subfault_dict in sipat_mks_dict["subfaults"]:
        if subfault_dict["is_valid"]:
            results = Okada.from_SIPAT_dict(NodeCoords, ElemNodes, Meter2Degrees,
                                                          subfault_dict, elastic_parameters_dict,
                                                          recompute_points=False)
            if results is not None:
                subfaults_W.append( results[0] )
                subfaults_HU.append( results[1] )
                subfaults_HV.append( results[2] )

    # Add the contributions
    W = sum(subfaults_W)
    HU = sum(subfaults_HU)
    HV = sum(subfaults_HV)

    # Write down the okada surface
    ic_path = os.path.join(target_path, "InitialConditions")
    Common.save_data(ic_path, "WaterLevel.txt", W)
    Common.save_data(ic_path, "DischargeX.txt", HU)
    Common.save_data(ic_path, "DischargeY.txt", HV)
    Common.save_data(ic_path, "Meter2Degrees.txt", Meter2Degrees)
    return


################################################################################
#
################################################################################
def _scenario_was_selected(line_number, sid, line_list, sid_list):
    """
    Checks if scenario was selected
    """
    if len(line_list)>0:
        select_line = (int(line_number) in line_list)
    elif len(sid_list)>0:
        select_line = (int(sid) in sid_list)
    else:
        select_line = True # Otherwise, take all lines
    return select_line


################################################################################
#
################################################################################
def _create_mks_dic(params_list, nu=Default.poisson_coefficient()):
    """
    load SIPAT files in defined format
    """
    deg2rad = np.pi/180. # To convert from degrees to radians
    km2m = 1.0E3         # To convert from kilometers to meters
    GPa2Pa = 1.0E9       # To convert from Gigapascals to pascals

    sipat_mks_dic = {}

    # Basic definition of dict for the event centroid
    # This will not be used to compute okada, as it is an "overall" information
    sipat_mks_dic["general_info"] = {
                                     "scID":_convert(params_list[0],int), # ID [], ejemplo 1334011
                                     "scMw":_convert(params_list[1],float), # Centroid Mw [Richter]
                                     "scLon":_convert(params_list[2],float, True),  # Centroid Longitude Epicenter [rad]
                                     "scLat":_convert(params_list[3],float), # Centroid Latitude Epicenter [rad]
                                     "scDepth":_convert(params_list[4],float)*km2m, # Centroid Depth [m]
                                     "scL":_convert(params_list[5],float)*km2m, # Centroid Depth [m]
                                     "scW":_convert(params_list[6],float)*km2m, # Centroid Depth [m]
                                     "scSlip":float(params_list[7]), # Centroid Slip [m], already in meters
                                     "scStrike":float(params_list[8])*deg2rad, # Centroid Strike [rad]
                                     "scDip":float(params_list[9])*deg2rad, # Centroid Dip [rad]
                                     "scRake":float(params_list[10])*deg2rad, # Centroid Rake [rad]
                                    }
    # Physical Parameters are computed and stored in a dictionary
    # All conversions made as indicated in https://en.wikipedia.org/wiki/Lam%C3%A9_parameters
    # Shear Modulus, indicated as Mu (also, and more commonly, G)
    Mu = float(params_list[11])*GPa2Pa
    # Lamme first coefficient (should be lambda, but thats a reserverd word in python)
    print("Using nu={0:.5f} (Poisson coefficient)".format(nu))
    Lambda = 2.*Mu*nu/(1.-2.*nu) # nu will be around 0.25, so no problem in diving by 0.
    sipat_mks_dic["elastic_parameters"] = {
                                           "lambda": Lambda, # First coefficient
                                           "mu": Mu, # Second coefficient
                                           "nu": nu, # Poisson Coefficient
                                          }
    # Physical Parameters
    sipat_mks_dic["number_of_subfaults"] = _convert(params_list[12],int) # Number of subfaults

    # Iterated definition of dict for the event subfaults
    sipat_mks_dic["subfaults"] = []
    for i in range(0,4):
        subfault_number = i
        start = 13 + 15*i
        sipat_mks_dic["subfaults"].append({})
        # Number for current subfault
        key = "scSub"
        sipat_mks_dic["subfaults"][subfault_number][key] = _convert(params_list[start+0], int)

        key = "scSubLon"
        # Subfault Longitude Epicenter [rad]
        sipat_mks_dic["subfaults"][subfault_number][key] = _convert(params_list[start+1], float, True)

        # Subfault Latitude Epicenter [rad]
        key = "scSubLat"
        sipat_mks_dic["subfaults"][subfault_number][key] = _convert(params_list[start+2], float)

        # Subfault Depth [m]
        key = "scSubDepth"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+3])*km2m

        # Subfault Strike [rad]
        key = "scSubStrike".format(subfault_number)
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+4])*deg2rad

        # Subfault Dip [rad]
        key = "scSubDip"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+5])*deg2rad

        # Subfault Rake [rad]
        key = "scSubRake"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+6])*deg2rad

        # Subfault Mu [Pa] [m]
        key = "scSubMu"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+7])*GPa2Pa

        # Subfault Length [m]
        key = "scSubL"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+8])*km2m

        # Subfault Width [m]
        key = "scSubW"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+9])*km2m

        # Subfault Slip [m]
        key = "scSubSlip"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+10]) # Already in m!!!

        # Rise Time (use rupture duration) for subfault
        key = "scSubTrise"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+11]) # Alreay in sec

        # Rupture initiaion time
        key = "scSubTini"
        sipat_mks_dic["subfaults"][subfault_number][key] = float(params_list[start+12]) # Alreay in sec

        # Recta = recta a la cual pertenece (solo para fallas simples) de la Subfalla 1 (??)
        key = "scSubRec"
        sipat_mks_dic["subfaults"][subfault_number][key] = _convert(params_list[start+13],int)

        # Area = Corresponde a ley de Escalamiento (solo para el mu=3) de la Subfalla (???)
        key = "scSubArea"
        sipat_mks_dic["subfaults"][subfault_number][key] = _convert(params_list[start+14],float)

        # Auxiliar variable: is a valid subfault (ie, all values are non-nan)?
        key = "is_valid"
        vals = np.array(sipat_mks_dic["subfaults"][subfault_number].values())
        sipat_mks_dic["subfaults"][subfault_number][key] = all(~np.isnan(vals))

    return sipat_mks_dic


################################################################################
#
################################################################################
def _convert(string, type_function, normalize_longitude=False):
    """
    Function that converts the data including the nan cases that might arise
    """
    if string.lower()=="nan":
        return np.nan
    # if not nan
    try:
        if normalize_longitude==False:
            return type_function(string)
        else:
            embed()
            lon = type_function(string)
            lon = LonLatHandler.longitude_to_range(lon)
            return lon

    except:
        print("Error. Lon/Lat conversion not performed.")
        return sys.exit(-1)


################################################################################
#
################################################################################
def _print_sorted_dict(dic):
    """
    dummy helper
    """
    for key in sorted(dic.keys()):
        print("{0}: {1}".format(key, dic[key]))
    return
