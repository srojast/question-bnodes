import numpy as np
from .. import Common
from .. import Default

def get_raw_values(grid_dic, terminal_BCs):
    """
    Example of user defined benchmark case
    """
    ################################################################################
    # References
    ################################################################################
    references = ["User defined - Put reference here"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "wall", "wall"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params =  {"Tmax":10.0,  # How many seconds to run
                    "g":Default.g(), # Value for gravity
                    "manning":0.0} # No manning

    # Discretization parameters
    xmin = -50.0  # [m]
    xmax = +50.0  # [m]
    ymin = -25.0  # [m]
    ymax = +25.0  # [m]
    B_min =-10.0  #[m] 
    W_max =+ 0.5  #[m]
    radius = 10.0 # [m]

    ################################################################################
    # Mesh
    ################################################################################
    # Get the mesh from --NElems or --dL
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    R = np.sqrt(x**2 + y**2)
    Z = np.exp(-(R/R.max())**2)
    B = np.array([x, y, B_min*Z]).T

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    R = np.sqrt(x**2 + y**2)
    Z = W_max * np.where(R<radius, 1.0, 0.0)
    W = np.array([x, y, Z]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])
  
    ################################################################################
    # Required Output
    ################################################################################
    output = {}
    # Movie Animation Step
    output["movie_config"] =  {"dt_save":0.5, "compute_floading_and_arrival_time":True}
    # Time Series Config
    x_TS = [0, 10]
    y_TS = [0, 0]
    output["TS_config"] = [x_TS, y_TS]
    t_TS = np.linspace(0, exec_params["Tmax"], 101)
    # Time Series Known Values
    output["TS_values"] = []
    # Spatial Profile Config 
    output["SP_config"] = []
    # Spatial Profile Known Values 
    SP_values = []
    output["SP_values"] = []

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "output":output, "BC_list":test_BC, 
            "references":references, "params":exec_params}
