import glob
import sys
import os

import COMCOTLoader
import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# COMCOT FORMAT
# KEEP UPDATED THIS DOC IF FORMAT CHANGES
# READ BEFORE CHANGING ANY FUNCTION
################################################################################
"""
COMCOT FORMAT
=================
We assume COMCOT format will be provided as it is specified in the next lines,
which resume the specifications of COMCOT v 1.7. Original specification
can be found at the following link:
http://ceeserver.cee.cornell.edu/pll-group/doc/COMCOT_User_Manual_v1_7.pdf

NUMBER FORMAT:
    Dot is used as decimal separator, as in 3.141592 or 4.2E+1

LONGITUDE AND LATITUDE FORMAT:
    Longitude is provided in the range [0,360] degrees, but it can
    also be provided in the range [-180,180] degrees.
    It will be (internally) moved to the range [-180, 180] and stored
    in that range.
    Latitude is always provided in the range [-90,90] degrees.

The folder must have the following file structure:

REQUIRED FILES:

    ./*.ctl   (Can have any name, but program will search the (first) ctl file).
        Plain text file with specific format.
        This file has the names of the bathymetry and okada files.
        Several lines are of particular interest and must be present.
        For the "General Parameters for Simulation":
            "Total run time (Wall clock, seconds)"
            "Time interval to Save Data    ( unit: sec )"
            "Output Zmax & TS (0-Max Z;1-Timeseries;2-Both)"
            "Specify BC  (0-Open;1-Sponge;2-Wall;3-FACTS)"
        For the "Parameters for Fault Model":
            "File Name of Deformation Data"
        For the "Configurations for all grids":
            "File Name of Bathymetry Data"
            "Manning's Roughness Coef. (For fric.option=0)"
            "FileName of Water depth data" (can appear several times)
        OBS: Only bathymetries and water depth data with "Data Format Option : 2"
        will be considered (this option 2 being XYZ) in longitude-latitude and meters.

    ./*.xyz (Can have any name, but name must be included in the main ctl file).
        Plain text files with the point coordinates and the corresponding bathymetry.
        There will be a main file, provided by "File Name of Bathymetry Data"
        and some additional layers provided by "FileName of Water depth data",
        which are specificed on the ctl file.
        No header.
        Each line has 3 values: the longitude, the latitude and the bathymetry/topography
        in meters.
        Sign convention: POSITIVE BELOW water level.
        (positive for bathymetry data and negative for topographical data)

OPTIONAL FILES:

    ./*.dat (Can have any name, but name must be included in the main ctl file).
        Plain text file with the point coordinates and the water level.
        No header.
        Each line has 3 values: the longitude, the latitude and the water level
        in meters. Water level points do not need to match the bathymetry points.
        Sign convention: positive over water level, negative below water level. (CHEEEECK!!!!!!)
        If not present, water surface assumed at level 0.

    ./ts_location.dat (Always the same name)
        Plain text file with the longitude and latitude positions of the (time series)
        buoys.
        No header.
        If not present, no time series buoys will be saved.

    ./sp_location.dat (Always the same name)
        Plain text file with the longitude and latitude positions of the (spatial profile)
        buoys.
        No header.
        If not present, no spatial profile buoys will be saved.
"""


################################################################################
# TESTS FOR COMCOT FILE FORMAT
################################################################################
def has_valid_format(fpath):
    """
    Check if provided path has comcot format.
    """
    if not os.path.exists(fpath):
        print("Folder {0} does not exists.".format(fpath))
        return sys.exit(-1)
    ctl_files = (fpath + "/*.ctl").replace("//","/")
    has_ctl_files = len(glob.glob(ctl_files))>0
    xyz_files = (fpath + "/*.xyz").replace("//","/")
    has_xyz_files = len(glob.glob(xyz_files))>0
    if not has_ctl_files:
        print("Missing the main file (ctl extension)")
        return False
    if not has_xyz_files:
        print("Missing the bathymetry file (xyz extension)")
        return False
    return has_ctl_files and has_xyz_files


################################################################################
# CONVERT
################################################################################
def load_as_sbf_dict(fpath):
    """
    Returns a sbf_dict as defined in SBFFormat.
    """
    if has_valid_format(fpath):
        return COMCOTLoader.load_as_sbf_dict(fpath)
