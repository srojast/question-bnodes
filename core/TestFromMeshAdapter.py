import numpy as np
import sys

import ArgumentHandler
import LonLatHandler
import MeshAdapter
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

def generate(options):
    """
    Generates the folder with answer files.
    **Input**:
    **Output**:
    """

    # The only valid arguments (not all simultaneously required)
    arguments = ["from_sbf", "to_sbf",
                 "adapt",
                 "exclude_range",
                 "remove_polygon",
                 "select_range"]
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)

    # Must have the to_folder
    arguments = ["from_sbf", "to_sbf", "adapt"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)

    # Check that at remove_polygon has correct input
    if options["remove_polygon"] is not None:
        if len(options["remove_polygon"])%2 != 0:
            print("The number of points provided to --remove_polygon must be even")
            print("Correct usage: --remove_polygon x1 y1 x2 y2 ... xn yn")
            sys.exit(-1)

    # Check that at most one exclusion is done
    valid_arguments = ["exclude_range", "remove_polygon"]
    if not ArgumentHandler.at_most_one_argument(options, valid_arguments):
        return sys.exit(-1)

    # Check for correct number of values in --adapt
    if len(options["adapt"])>3:
        print("Too many values provided to --adapt")
        print("Only Niters and h0 must be provided")
        return sys.exit(-1)

    # Parameters for calling the mesh adaptr
    from_folder = options["from_sbf"]
    to_folder = options["to_sbf"]
    adapt_dict = {}
    adapt_dict["select_bbox"] = range_to_bbox(options["select_range"], "select_range")

    #Get the exclude polygon from range or polygon
    adapt_dict["remove_polygon"] = get_remove_polygon(options)

    # Add the hmin
    if len(options["adapt"])==3:
        adapt_dict["hmin_mks"] = float(options["adapt"][2])
        adapt_dict["dt_mks"] = float(options["adapt"][1])
        adapt_dict["Niters"] = int(options["adapt"][0])
    else:
        adapt_dict["Niters"] = int(options["adapt"][0])


    # Everything should be ok now, so run execution
    # Run the test with the given options
    MeshAdapter.generate(from_folder, to_folder, adapt_dict)

    # Print the usage for the benchmark
    Common.print_sbf_usage(to_folder)
    return sys.exit(0)

def range_to_bbox(user_provided_range, range_name):
    """
    Converts from range to bounding box, as required for algorithms
    """
    if user_provided_range:
        XRangeMin, XRangeMax, YRangeMin, YRangeMax = user_provided_range
        if XRangeMin>=XRangeMax:
            print("Min for x (or longitude) is greater than Max for x (or latitude)")
            print("Check range provided for %s" %range_name)
            sys.exit(-1)
        if YRangeMin>=YRangeMax:
            print("LatMin is greater than LatMax in --exclude_range.")
            sys.exit(-1)
        bbox = [XRangeMin, YRangeMin, XRangeMax, YRangeMax] # in bbox format
    else:
        bbox = []
    return bbox

def get_remove_polygon(options):
    """
    Returns a polygon (ordered list of points from options)
    """
    if options["exclude_range"] is not None:
        return range_to_bbox(options["exclude_range"], "exclude_range")
    else:
        if options["remove_polygon"] is not None:
            n = len(options["remove_polygon"])
            remove_polygon = np.array(options["remove_polygon"]).reshape(n/2,2)
            return remove_polygon
        else:
            return None
