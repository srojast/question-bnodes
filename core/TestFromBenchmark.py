import sys

import ArgumentHandler
import Benchmarks
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

def generate(options):
    """
    Generates the folder with answer files
    **Input**:
    **Output**:
    """

    # Arguments that are accepted for benchmark (all others rejected)
    arguments = ["benchmark", "from", "to", "dL", "NElems", "BC", "BC_list", "triangles", "Bnodes"]
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)

    # Must have the to_folder
    arguments = ["to", "benchmark"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)

    # Check the grid options
    valid_arguments = ["dL", "NElems"]
    if not ArgumentHandler.at_most_one_argument(options, valid_arguments):
        return sys.exit(-1)

    # Check Boundary Conditions
    valid_arguments = ["BC", "BC_list"]
    if not ArgumentHandler.at_most_one_argument(options, valid_arguments):
        return sys.exit(-1)

    # Create the BC list, abort execution if not possible
    BC_list = ArgumentHandler.BC_from_options(options)
    if not ArgumentHandler.valid_BC_list(BC_list):
        return sys.exit(-1)

    # Create the grid dic
    grid_dic = ArgumentHandler.benchmark_grid_from_options(options)
     # Everything should be ok now, so run execution
    # Run the test with the given options
    saveBnodes = options["Bnodes"]
    to_folder = options["to"].strip()
    benchmark_number = options["benchmark"]
    Benchmarks.generate(to_folder,
                        benchmark_number,
                        grid_dic,
                        BC_list,
			saveBnodes)

    # Print the usage for the benchmark
    Common.print_usage(to_folder)

    return
