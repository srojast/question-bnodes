from scipy import interpolate
import numpy as np
import itertools
import os

import Default
import Output

if Default.DEBUG_MODE:
    from IPython import embed

#CONVERT_LONGITUDES_TO_RANGE = True # True or False

def geographical_to_cartesian(LonLatArray, LinearizationPoint):
    """
    Converts the given array of positions in Lat/Lon,
    to position in meters using the Linearization Point.
    It assumes the Longitudes and Latitudes in degrees.
    OBS: To work properly, LonLatArray must be in a continuous range,
    with no jumps.
    """
    # Parameters
    R = 6378140.0 # [m]
    deg2rad = np.pi/180.
    # Return if there's nothing to be converted
    if not len(LonLatArray.shape)==2:
        return LonLatArray
    # Unpack in degrees
    Lon, Lat = LonLatArray[:,0], LonLatArray[:,1]
    Lon0, Lat0 = LinearizationPoint[0], LinearizationPoint[1]
    # Compute the delta angle in radians
    dLat_rad = (Lat-Lat0)*deg2rad
    dLon_rad = (Lon-Lon0)*deg2rad
    # Convert Latitudes
    Y = R * dLat_rad
    # Convert Longitudes
    r = R * np.cos(Lat * deg2rad)     # Minor radius at Latitude for linearization
    X = r * dLon_rad
    # XYArray
    XYArray = np.array([X,Y]).T
    return XYArray

def cartesian_to_geographical(XYArray, LinearizationPoint):
    """
    Converts the given array of positions in cartesian/meters,
    to position in LonLat using the Linearization Point.
    It assumes the Longitudes and Latitudes in degrees.
    """
    # Parameters
    R = 6378140.0 # [m]
    rad2deg = 180./np.pi
    # Unpack
    X, Y = XYArray[:,0], XYArray[:,1]
    Lon0, Lat0 = LinearizationPoint[0], LinearizationPoint[1]
    # Compute the Latitude
    Lat_deg = (Y/R)*rad2deg + Lat0
    # Compute the Longitude
    r = R * np.cos(Lat_deg/rad2deg)
    Lon_deg = (X/r)*rad2deg + Lon0
    # XYArray
    LonLatArray = np.array([Lon_deg,Lat_deg]).T
    return LonLatArray


################################################################################
# SPECIALIZED SAVER FOR COORDINATE SYSTEM
################################################################################
def save_coordinate_system(cs_filepath, coordinate_system, linearization_point):
    if coordinate_system == "cartesian":
        with open(cs_filepath,"w") as cs_writer:
            Lon0, Lat0 = linearization_point
            cs_writer.write("# Cartesian Coordinate System\n")
            cs_writer.write("# Using: projection from sphere into plane.\n")
            cs_writer.write("# Linearization point (Longitude, Latitude):")
            cs_writer.write("{0:.9f}, {1:.9f}\n".format(Lon0, Lat0))
    else:
        with open(cs_filepath,"w") as cs_writer:
            cs_writer.write("# Longitude [degrees], Latitude [degrees]\n")
    return


################################################################################
# SPECIALIZED READER AND SAVER FOR COORDINATE SYSTEM
################################################################################
def load_coordinate_system(cs_filepath):
    # We asume by default that the file is in cartesian/meters.
    coordinate_system = "cartesian"
    linearization_point = None
    with open(cs_filepath, "r") as filereader:
        for line in filereader.readlines():
            sanitized_line = line.lower().strip()
            if "linearization" in sanitized_line:
                values = sanitized_line.split(":")[1]
                lon_str, lat_str = values.split(",")
                linearization_point = np.array([float(lon_str), float(lat_str)])
                return coordinate_system, linearization_point
            if "longitude" in sanitized_line and "latitude" in sanitized_line:
                coordinate_system = "geographical"
                linearization_point = None
                return coordinate_system, linearization_point
    return coordinate_system, linearization_point

################################################################################
# normalize_longitude
################################################################################
def normalize_longitude(lon):
    print "normalize_longitude: TO DO"
    return lon
