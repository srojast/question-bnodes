import numpy as np
import glob
import sys
import os

import LonLatHandler
import QASLoader
import QASSaver
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

"""
benchmark_dict: Has the following keys:
 * "initial_values": dict {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
 * "execution_parameters":
 * "references": list of strings ["ref1", ..., "refn"]
 * "movie": dict {"compute":True, "dt_save":0.50}
 * inundation": {"compute":False}
 * "heatmap": {"compute":False}
 * "arrival_time": {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}
 * "time_series": {"points", "dt_save", "measured_values"}
 * "spatial_profile": {"points", "sampling_times", "measured_values"}
"""

"""
 qas_dict: Has the following keys:
 * "initial_values": dict {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
 * "execution_parameters":
 * "references": list of strings ["ref1", ..., "refn"]
 * "movie": dict {"compute":True, "dt_save":0.50}
 * inundation": {"compute":False}
 * "heatmap": {"compute":False}
 * "arrival_time": {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}
 * "time_series": {"points", "dt_save", "measured_values"}
 * "spatial_profile": {"points", "sampling_times", "measured_values"}
"""
################################################################################
# WRITE README FILE FOR CREATED FOLDER
# KEEP UPDATED THIS DOC IF FORMAT CHANGES
# READ BEFORE CHANGING ANY FUNCTION
################################################################################
def write_readme(to_path):
    readme = """QAS FORMAT\n=================
QAS format is the format assumed by QUESTION, ANSWER and SWEET.

NUMBER FORMAT:
    Dot is used as decimal separator, as in 3.141592 or 4.2E+1

The folder must have the following file structure:

REQUIRED MESH FILES:

    ./Mesh/ElemNeighs.txt
        Plain text file with the connectivity of the mesh.
        Has a header with dimensions of the array, which should be (NElems, 3)
        Line i has a 3 integers, the (ordered) neighbors of the cell i.
        All integers must be non-negative and cell numbering start in zero.

    ./Mesh/ElemNeighSides.txt

        Has a header with dimensions of the array, which should be (NElems, 3)

        All integers must be non-negative and cell numbering start in zero.

    ./Mesh/ElemNodes.txt
        Plain text file with the nodes index for each cell.
        Has a header with dimensions of the array, which should be (NNElems, 3)
        Line i has a 3 integers, the node index of the cell i.
        All integers must be non-negative and cell numbering start in zero.

    ./Mesh/ElemSides.txt
        Has a header with dimensions of the array, which should be (NNElems, 3)
        All integers must be non-negative and cell numbering start in zero.

    ./Mesh/GhostCells.txt
        Plain text file with the labeling of the Ghost Cells.
        Has a header with dimensions of the array, which should be (NGhostCells, 3)
        Line i (Ghost Cell i) has a 3 integers:

    ./Mesh/NodeCoords.txt
        Plain text file with the values (in meters) of the node coordinates.
        Has a header with dimensions of the array, which should be (Nnodes, 2)
        Each line has a node x and y coordinates (in meters), separated by space.

    ./Mesh/CoordinateSystem.txt
        Text file with the coordinate system used to express the coordinates.
        Same as in SBFFormat, but can only be cartesian. Used to keep track of
        linearization point used (if any).

OPTIONAL MESH FILES:

    ./Mesh/Optional/Meter2Degrees.txt
        Plain text file with the Water Level values at the mesh coordinates.
        Each line has a unique value of water level (in meters),
        for the node of the corresponding line of the mesh file.
        Sign convention: positive over water level, negative below water level.

    ./Mesh/original_unrefined_variables.pkl
        Pickle file that is present only when the mesh has been generated with
        the mesh refiner. It allows to relaunch the mesh refiner from the
        last iteration and keep refining without restarting from zero.

REQUIRED INITIAL CONDITION FILES:

    ./Mesh/Bathymetry.txt
        Plain text file with the Bathymetry, assumed constant within each cell.
        Has a header with dimensions of the array, which should be (NElems, 1)
        Each line has a unique value of bathymetry (in meters),
        for the node of the corresponding line of the ElemNodes file.
        Sign convention: positive for z-direction (over water level),
        negative below water level.

    ./Mesh/DischargeX.txt
        Plain text file with the Discharge in the x-direction, assumed constant
        within each cell.
        Has a header with dimensions of the array, which should be (NElems,1)
        Line i+1 has the value of dicharge=velocity*water_height
        (in meters^2/second), for the cell i.
        Sign convention: positive when water moves in the positive x-direction.

    ./Mesh/DischargeY.txt
        Plain text file with the Discharge in the x-direction, assumed constant
        within each cell.
        Has a header with dimensions of the array, which should be (NElems,1)
        Line i+1 has the value of dicharge=velocity*water_height
        (in meters^2/second), for the cell i.
        Sign convention: positive when water moves in the positive x-direction.

    ./Mesh/WaterLevel.txt
        Plain text file with the Water Level, assumed constant within each cell.
        Has a header with dimensions of the array, which should be (NElems,1)
        Line i+1 has the value of water level (in meters), for the cell i.
        Sign convention: positive for z-direction (over water level),
        negative below water level (Same as Bathymetry).

REQUIRED OUTPUT FILES:
    ./Output/ExecutionParameters.txt
        Plain text file with the parameters to be used on answer when running
        the simulation.
        It has a specifi notation, but file can be opened, altered and explored
        directly.
        It has higher precedence over other files. If Time Series or
        Spatial Profiles configuration variables (SaveTimeSeries and
        SaveSpatialProfiles, respectively) are False, ANSWER will not compute
        the respective values, regardless of the respective configuration files
        being available.

    ./Output/SpatialProfilesConfiguration.txt
        Plain text file with a specific format to save spatial profiles.
        Line 0: header for the time array, which should be (1,NTimesSpatialProfile)
        Line 1: times where data should be stored. should be NTimesSpatialProfile
        values separated by commas.
        Line 2: header for the bouys that need to be saved at the specific times.
        Should be in format (3, NSpatialProfile).
        Line 3+i : x and y positions (both floats), and the cell index
        (non negative int), for the buoy i of Spatial Profile.

    ./Output/TimeSeriesConfiguration.txt
        Plain text file with the values (in meters) of the buoy coordinates
        and the corresponding cell index for that coordinates.
        Has a header with dimensions of the array, which should be (NTimeSeries, 3)
        Line i has 3 values: the x and y positions (both floats), and
        the cell index (non negative int).

OPTIONAL OUTPUT FILES:

    ./Output/SpatialProfilesReferenceValues.txt
        Plain text file with the recorded values for the Spatial Profiles.
        Line 0: Header for the number NSP of profiles taken.
        From line 1, the lines are
         Just a value, the studied time
         The values for the Spatial Profile Buoys.
        Time Sign convention: tsunami occurs at 0 seconds.
        Water Level Sign convention: positive over water level, negative below water level.

    ./Output/TimeSeriesReferenceValues.txt
        Plain text file with the recorded values for the Time Series.
        Line 0: Header for the number NTS of Time Series taken.
        From line 1, the lines are
         Array of times.
         The values for the a Buoy.
        Time Sign convention: tsunami occurs at 0 seconds.
        Water Level Sign convention: positive over water level, negative below water level.
"""
    with open(os.path.join(to_path, "README.txt"), "w") as fw:
        fw.write(readme)
    return

################################################################################
# TESTS FOR THE PICKLED QUESTION-ANSWER-SWEET FILE FORMAT
################################################################################
def has_valid_format(fpath):
    if not os.path.exists(fpath):
        print("Folder {0} does not exists.".format(fpath))
        return sys.exit(-1)
    # Boolean for question structure
    question_folders = ["Mesh", "InitialConditions", "Output"]
    in_question_format = all( os.path.exists(os.path.join(fpath,folder)) for folder in question_folders)
    return in_question_format

################################################################################
# SAVE DATA TO PATH ASSUMING DATA IS PASSED WITH A SBF_DICT
################################################################################
def save_from_sbf_dict(to_path, sbf_dict,
                       saveBnodes,
                       domain_points=None,
                       exclude_points=None):
    QASSaver.save_from_sbf_dict(to_path, sbf_dict, saveBnodes, domain_points, exclude_points)
    return

################################################################################
# SAVE DATA TO PATH ASSUMING DATA IS PASSED WITH A BENCHMARK_DICT
################################################################################
def save_from_benchmark_dict(to_path, benchmark_dict, saveBnodes):
    QASSaver.save_from_benchmark_dict(to_path, benchmark_dict, saveBnodes)
    return

################################################################################
# SAVE DATA TO PATH ASSUMING DATA IS PASSED WITH A QAS_DICT
################################################################################
def save_from_qas_dict(to_path, qas_dict):
    QASSaver.save_from_qas_dict(to_path, qas_dict)
    return

################################################################################
# LOAD DATA FROM PATH ASSUMING QAS FORMAT
################################################################################
def load(from_path):
    print("Reading files in QAS format.")
    # COPY FROM COMMON
    return
