from scipy.interpolate import griddata
import numpy as np
import sys
import os

import ArgumentHandler
import LonLatHandler
import Default
import Common
import Output
import Mesh

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
################################################################################
def generate(options):
    """
    Overwrite the Initial Water Surface using the
    flat/bell/wave or a surface file
    provided by the user
    """
    # Arguments that are accepted for --flat, --bell, --wave (all others rejected)
    arguments = ["from", "to"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)
    options["to"] = options["to"].strip()
    options["from"] = options["from"].strip()

    # Check the provided options
    valid_arguments = ["flat", "bell", "wave", "surface_from_file"]
    if not ArgumentHandler.only_one_argument(options, valid_arguments):
        return sys.exit(-1)

    # Get the values
    from_folder = os.path.abspath(options["from"])
    to_folder = os.path.abspath(options["to"])

    # Create a new folder if need to, overwrite otherwise
    if to_folder==from_folder:
        pass
    else:
        Common.copy(from_folder, to_folder)

    # Read the files
    ic_path = os.path.join(to_folder, "InitialConditions")
    mesh_path = os.path.join(to_folder, "Mesh")

    Bj = np.loadtxt(os.path.join(ic_path,"Bathymetry.txt"))
    NodeCoords = np.loadtxt(os.path.join(mesh_path,"NodeCoords.txt"))
    ElemNodes = np.loadtxt(os.path.join(mesh_path,"ElemNodes.txt"), dtype = int)

    # Impose the surface using the provided options
    if options["surface_from_file"] is not None:
        surface_file, surface_file_CS = options["surface_from_file"]
        folder_CS = os.path.join(mesh_path, "CoordinateSystem.txt")
        if os.path.exists(surface_file):
            Wj, HUj, HVj = from_file(NodeCoords, ElemNodes, Bj,
                                     surface_file, surface_file_CS, folder_CS)
        else:
            print("Provided file does not exists.")
            sys.exit(-1)
    else:
        # Create surface according to keyword
        custom_water_level = ArgumentHandler.custom_water_level_dic_from_options(options)
        # Get the Water Level with giving options
        if custom_water_level["type"]=="flat":
          Wj,HUj,HVj = flat(NodeCoords, ElemNodes, Bj, *custom_water_level["parameters"])
        if custom_water_level["type"]=="bell":
          Wj,HUj,HVj = bell(NodeCoords, ElemNodes, Bj, *custom_water_level["parameters"])
        if custom_water_level["type"]=="wave":
          Wj,HUj,HVj = wave(NodeCoords, ElemNodes, Bj, *custom_water_level["parameters"])

    # Overwrite W similar values
    Common.save_data(ic_path, "WaterLevel.txt", Wj)
    Common.save_data(ic_path, "DischargeX.txt", HUj)
    Common.save_data(ic_path, "DischargeY.txt", HVj)

    # Print the usage for the benchmark
    Common.print_usage(options["to"])
    return

################################################################################
# Loads from file and interpolates according to mesh
################################################################################
def from_file(NodeCoords, ElemNodes, Bj, surface_file, surface_file_CS, folder_CS):
    # Loading (file has been already checked for existance)
    given_xyW = np.loadtxt(surface_file)
    # Read surface_file_CS
    CS, LP = LonLatHandler.load_coordinate_system(folder_CS)
    # OBS: CS can only be cartesian, because we-re on QAS format.
    # Convert surface, if requested
    header = open(surface_file).readline().rstrip().lower()
    if surface_file_CS=="geographical":
        print("Converting given surface from Longitude and Latitude to meters")
        given_xyW[:,:2] = LonLatHandler.geographical_to_cartesian(given_xyW[:,:2], LP)
    else:
        print "Assuming provided surface is already in cartesian coordinate system"

    # Check we will have enough points for griddata
    if given_xyW.shape[0]<3:
        print("Not enough data points for interpolation")
        sys.exit(-1)
    # Get each component for the grid data
    x = given_xyW[:,0]
    y = given_xyW[:,1]
    z = given_xyW[:,2]
    # Get the original mesh
    xj, yj = Common.get_centroids(NodeCoords, ElemNodes)
    #print xj.max(), xj.min()
    points = np.array([xj,yj]).T
    # Linear interpolation inside common domain
    Wj  = griddata((x,y), z, points, method = 'linear')
    # Nearest extrapolation ouside common domain.
    # Use the current values to continue with the linearity inside domain
    nans = np.isnan(Wj)
    not_nans = np.logical_not(nans)
    if nans.sum()>0:
        if not_nans.sum()>0:
            """
            print("Using interpolation for points inside provided surface")
            print("Applying extrapolation of created points for points outside provided surface")
            # Extrapole from data already interpoled to get linearity in solutions
            nearextrap = griddata((xj[not_nans],yj[not_nans]), Wj[not_nans],
                                  points[nans,:], method='nearest')
            Wj[nans] = nearextrap
            """
            print("Zeroing on unknown data")
            # If no data was interpoled, all to do is extrapolation
            Wj[nans] = 0.0
        else:
            """
            print("Applying extrapolation for points outside provided surface")
            # If no data was interpoled, all to do is extrapolation
            nearextrap = griddata((x,y), z,
                                  points[nans,:], method='nearest')
            Wj[nans] = nearextrap
            """
            print("Zeroing on mesh points ouside surface file ")
            # If no data was interpoled, all to do is extrapolation
            Wj[nans] = 0.0
    # Fix bathymetry if required
    Wj = np.maximum(Wj, Bj) # Water level can never be below the bathymetry
    # Discharges
    HUj = np.zeros(Wj.shape)
    HVj = np.zeros(Wj.shape)
    return Wj, HUj, HVj

################################################################################
# Defines a bell of water in a flat water level
################################################################################
def flat(NodeCoords, ElemNodes, Bj, W0, U0, V0):
    print("Imposing new water surface using:")
    print("\tW0={:.2f}, U0{:.2f} and V0={:.2f}".format(W0, U0, V0))
    xj, yj = Common.get_centroids(NodeCoords, ElemNodes)
    Wj = W0 + 0*xj
    # Fix bathymetry if required
    Wj = np.maximum(Wj, Bj) # Water level can never be below the bathymetry
    Hj = Wj-Bj
    # Compute the discharges
    HUj = Hj*U0
    HVj = Hj*V0
    return Wj, HUj, HVj

################################################################################
# Defines a bell of water in a flat water level
################################################################################
def bell(NodeCoords, ElemNodes, Bj, X0, Y0, Hmax, Rmax):
    print("Imposing new water surface using:")
    print("\t(X0,Y0)=({:.2f}, {:.2f}), H={:.2f} and R={:.2f}".format(X0, Y0, Hmax, Rmax))
    xj, yj = Common.get_centroids(NodeCoords, ElemNodes)
    N = len(xj) # number of elements
    r = np.sqrt((xj-X0)**2 + (yj-Y0)**2)
    bump = Hmax*(Rmax**2-r**2)/Rmax**2
    Wj = np.where(r<Rmax, bump, 0)
    # Fix bathymetry if required
    Wj = np.maximum(Wj, Bj) # Water level can never be below the bathymetry
    # Discharges
    HUj = np.zeros(Wj.shape)
    HVj = np.zeros(Wj.shape)
    return Wj, HUj, HVj

################################################################################
# Defines a bell of water in a flat water level
################################################################################
def wave(NodeCoords, ElemNodes, Bj, X0, Y0, Hmax, Wmax, theta_deg):
    print("Imposing new water surface using:")
    print("\t(X0,Y0)=({:.2f}, {:.2f}), H={:.2f}, W={:.2f},  and theta={:.2f} [deg]".format(X0, Y0, Hmax, Wmax, theta_deg))
    xj, yj = Common.get_centroids(NodeCoords, ElemNodes)
    hw = .5*Wmax
    theta_rad = 2.0*np.pi/360 * theta_deg
    lines = ((yj-Y0)-np.tan(theta_rad)*(xj-X0))/hw
    shape = Hmax*np.cos(.5*np.pi*lines)
    where = np.abs(lines) < 1.0
    zj = np.zeros(xj.shape) + shape*where
    Wj = zj
    # Fix bathymetry if required
    Wj = np.maximum(Wj, Bj) # Water level can never be below the bathymetry
    # Discharges
    print "Fix Discharges so it can propagate as a stationary wave"
    HUj = np.zeros(Wj.shape)
    HVj = np.zeros(Wj.shape)
    return Wj, HUj, HVj
