import numpy as np
from .. import Common
from .. import Default

def get_raw_values(grid_dic, terminal_BCs):
    """
    This debugging case represents a still water level  on a flat bathymetry. The case generates a triangular mesh in which 
    well-balanced property for wet case is tested. An initial water level surface can also be applied.
    """

    # References:
    references = []

    # Suggested parameter usage:
    test_default_BCs = ["periodic", "periodic", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params = {"Tmax":60.0, "g":Default.g(), "manning":0.0}

    # Physical Parameters
    depth    = 20.0 # [m]

    # Discretization parameters
    xmin = -100.0 # [m] 
    xmax =  100.0 # [m]
    ymin = -100.0 # [m]
    ymax =  100.0 # [m]

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = depth
    grid_dic["u_mean"] = 0.
    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x,y]).T

    ################################################################################
    # Bathymetry (B)
    ################################################################################
    B = np.array([-depth])

    ################################################################################
    # Water Level Surface (W)
    ################################################################################
    W = np.array([0.0])

    ################################################################################
    # Flux Velocities (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    ################################################################################
    # Required Output
    ################################################################################
    output = {}
    # Movie Animation Step
    output["movie_config"] = {"dt_save":1.0, "compute_floading_and_arrival_time":True}
    # Time Series Config and Values
    output["TS_config"] = []
    output["TS_values"] = []
    # Spatial Profile Config and Values
    output["SP_config"] = []
    output["SP_values"] = []

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, 
            "output":output, 
            "BC_list":test_BC, 
            "references":references, 
            "params":exec_params}
