import numpy as np
from .. import Common
from .. import Default

def get_raw_values(grid_dic, terminal_BCs):
    """
    This debugging case represents a still water configuration on a flat bathymetry. The mesh is non regular. The case generates a 
    triangular mesh in which well-balanced property for wet case is tested. An initial water level surface can be also be applied.
    """

    # References:
    references = []

    # Suggested parameter usage:
    test_default_BCs = ["periodic", "periodic", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params = {"Tmax":60.0, "g":Default.g(), "manning":0.0, "tol_dry":Default.tol_dry()}

    # Physical Parameters
    depth    = 20.0 # [m]

    # Discretization parameters
    xmin = -100.0 # [m] 
    xmax =  100.0 # [m]
    ymin = -100.0 # [m]
    ymax =  100.0 # [m]

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = depth
    grid_dic["u_mean"] = 0.
    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)

    # Convenient wrapper for the f(x) = lambda x: np.sin(.5*np.pi*x)
    f = lambda x: (np.tan(0.25*np.pi*x**3)/np.tan(0.25*np.pi))

    # Scale so as to get a non regular grid. Any function that maps smoothly [-1,1] into [-1,1]
    thx = (2*x - xmax - xmin)/(xmax - xmin)
    fx  = f(thx)
    x   = 0.5*xmin*(1 - fx) + 0.5*xmax*(1 + fx)

    thy = (2*y - ymax - ymin)/(ymax - ymin)
    fy  = f(thy)
    y   = 0.5*ymin*(1 - fy) + 0.5*ymax*(1 + fy)

    Mesh = np.array([x,y]).T

    ################################################################################
    # Bathymetry (B)
    ################################################################################
    B = np.array([-depth])

    ################################################################################
    # Water Level Surface (W)
    ################################################################################
    W = np.array([0.0])

    ################################################################################
    # Flux Velocities (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    # Pack
    simulation = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}

    ################################################################################
    # Required Output
    ################################################################################
    # Movie Animation Step
    movie = {"compute":True, "dt_save":20.0}

    # inundation map, heat map and arrival times maps
    inundation = {"compute":False}
    heatmap = {"compute":False}
    arrival_time = {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Config and Values
    TS = {"compute":False}
    # Spatial Profile Config and Values
    SP = {"compute":True, "points":[[-10.,-10.],[10,10]], "sampling_times":[5.,15.]}
    SP["measured_values"] = {"sampling_times":[6.,16.], "Wj":[[0, 10], [2, 12]]}
    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"simulation":simulation,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
