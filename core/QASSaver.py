from scipy import interpolate
import numpy as np
import itertools
import shutil
import errno
import os

import LonLatHandler
import QASFormat
import Default
import Output
import Common
import Mesh

if Default.DEBUG_MODE:
    from IPython import embed

'''
WARNING: Do not use this library directly. Use SBFFormat instead.
'''


################################################################################
# General Saving procedure
################################################################################
def save_from_qas_dict(folder_path, qas_dict, saveBnodes):
    """
    Saves the qas_dict into the provided folder_path,
    using the convention taken for ANSWER.
    """
    print("Save files in QAS format.")

    mesh_path = os.path.join(folder_path, "Mesh")
    output_path = os.path.join(folder_path, "Output")
    ic_path = os.path.join(folder_path, "InitialConditions")

    # Create the required folders
    Common.make_folders(folder_path)

    # Save the Mesh
    initial_values = qas_dict["initial_values"]
    Common.save_data(mesh_path, "NodeCoords.txt", initial_values["NodeCoords"])
    Common.save_data(mesh_path, "ElemNodes.txt", initial_values["ElemNodes"], fmt="%d")
    Common.save_data(mesh_path, "ElemNeighs.txt", initial_values["ElemNeigh"], fmt="%d")
    Common.save_data(mesh_path, "ElemNeighSides.txt", initial_values["ElemNeighSides"], fmt="%d")
    Common.save_data(mesh_path, "ElemSides.txt", initial_values["ElemSides"], fmt="%d")
    Common.save_data(mesh_path, "GhostCells.txt", initial_values["GhostCells"], fmt="%d")

    # Save the Coordinate System
    cs_filepath = os.path.join(mesh_path, "CoordinateSystem.txt")
    if "linearization_point" in qas_dict:
        linearization_point = qas_dict["linearization_point"]
        LonLatHandler.save_coordinate_system(cs_filepath, "cartesian", linearization_point)

    # Save the Initial Conditions
    Common.save_data(ic_path, "Bathymetry.txt", initial_values["Bj"])
    Common.save_data(ic_path, "WaterLevel.txt", initial_values["Wj"])
    Common.save_data(ic_path, "DischargeX.txt", initial_values["HU0j"])
    Common.save_data(ic_path, "DischargeY.txt", initial_values["HV0j"])
    if saveBnodes:
	Common.save_data(ic_path, "Bathymetry_at_Nodes.txt", initial_values["Bjk"])

    # Specify the execution parameters
    Output.save_execution_parameters(output_path, "ExecutionParameters.txt", qas_dict)

    # Specify the required output
    Output.save_time_series_configuration(output_path, "TimeSeriesConfiguration.txt", qas_dict["time_series"])
    Output.save_time_series_values(output_path, "TimeSeriesReferenceValues.txt", qas_dict["time_series"])
    Output.save_spatial_profiles_configuration(output_path, "SpatialProfilesConfiguration.txt", qas_dict["spatial_profile"])
    Output.save_spatial_profiles_values(output_path, "SpatialProfilesReferenceValues.txt", qas_dict["spatial_profile"])

    # Save readme file
    QASFormat.write_readme(folder_path)

    # Acknowledge everything is OK
    print "Files have been successfully generated.\n"

    # Print the references
    Common.print_references(qas_dict["references"])

    return

################################################################################
# Saving from a benchmark dict
################################################################################
def save_from_benchmark_dict(to_path, benchmark_dict,
                       saveBnodes,
                       domain_points=None,
                       exclude_points=None):
    # Perform the delaunay on the test values
    qas_dict = Mesh.generate(benchmark_dict, saveBnodes, domain_points, exclude_points)
    # Save the Mesh
    save_from_qas_dict(to_path, qas_dict, saveBnodes)
    return

################################################################################
# Saving from a sbf dict
################################################################################
def save_from_sbf_dict(to_path, sbf_dict,
                       saveBnodes,
                       domain_points=None,
                       exclude_points=None):
    benchmark_dict = {}
    # Pack the initial values
    initial_values = {}
    initial_values["Mesh"] = sbf_dict["Mesh"]
    initial_values["B"] = np.array([sbf_dict["Mesh"][:,0], sbf_dict["Mesh"][:,1], sbf_dict["Bathymetry"]]).T
    initial_values["W"] = np.array([0.0])
    initial_values["U0"] = np.array([0.0])
    initial_values["V0"] = np.array([0.0])
    initial_values["BC_list"] = ["soft"] * 4
    benchmark_dict["initial_values"] =  initial_values
    # Linearization Point
    benchmark_dict["linearization_point"] = sbf_dict["LinearizationPoint"]
    # References
    benchmark_dict["references"] = ["Converted from SBF files"]
    # Execution parameters
    Tmax = Default.tsunami_simulation_Tmax(nhours=8)
    benchmark_dict["execution_parameters"] = {"Tmax":Tmax,
                                              "g":Default.g(),
                                              "tol_dry":Default.tol_dry(),
                                              "manning":Default.manning()}
    # Movie Animation Step
    hourly = 60*60. # segs
    benchmark_dict["movie"] = {"compute":False, "dt_save": hourly/6.}
    # Inundation
    benchmark_dict["inundation"] = {"compute":True}
    # Heat map
    benchmark_dict["heatmap"] = {"compute":True}
    # Arrival Time
    benchmark_dict["arrival_time"] = {"compute":True, "height_threshold":Default.arrival_time_height_threshold()}
    # Time Series Config and Values
    if "TimeSeriesBuoyCoordinates" in sbf_dict:
        benchmark_dict["time_series"] = {"compute":True,
                                         "points":sbf_dict["TimeSeriesBuoyCoordinates"],
                                         "dt_save":1.0}
    else:
        benchmark_dict["time_series"] = {"compute":False}
    # Spatial Profile Config and Values
    if "SpatialProfileBuoyCoordinates" in sbf_dict:
        benchmark_dict["spatial_profile"] = {"compute":True,
                                             "points":sbf_dict["SpatialProfileBuoyCoordinates"],
                                             "sampling_times":np.linspace(0,Tmax,8*12)} # Each 5 minutes
    else:
        benchmark_dict["spatial_profile"] = {"compute":False}
    # Save using library
    save_from_benchmark_dict(to_path, benchmark_dict, saveBnodes, domain_points, exclude_points)
    return
