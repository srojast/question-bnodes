from scipy import interpolate
import numpy as np
import itertools
import shutil
import errno
import os

import LonLatHandler
import QASFormat
import Output
import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
################################################################################
def get_centroids(NodeCoords, ElemNodes):
    """
    Get centroids of the Elements.
    **Input**: The NodeCoords (NNodes,2) and ElemNodes (NElems, 3)
    **Output**: x and y vectors of coordinates.
    """
    xj = NodeCoords[ElemNodes,0].mean(axis=1)
    yj = NodeCoords[ElemNodes,1].mean(axis=1)
    return xj, yj

################################################################################
################################################################################
def copy(src, dest):
    """
    Copy from source src to destination dest, for files and folders.
    **Input**: strings src and dest, being both files or folders.
    **Output**:
    """
    # If same folder, do not copy, it already has the correct files
    if src==dest:
        return
    # If not the same folder, force copy
    try:
        if os.path.exists(dest):
            print("\tDestination folder already exists. Overwritting files.")
            shutil.rmtree(dest)
        shutil.copytree(src, dest)
    # If there was an error because the source wasn't a directory,
    # try to copy as a file
    except OSError as e:
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        # Unavoidable error
        else:
            print('Directory was not copied. Error: %s' % e)

################################################################################
# print test name from file name
################################################################################
def assign_BC(user_BC_list, test_default_BC):
    """
    Assigns the user BC unless user has not given BC, in which case
    it uses the test default BC
    """
    if user_BC_list is None:
	return test_default_BC
    else:
	return user_BC_list

################################################################################
# Print test name from file name
################################################################################
def test_name(filename):
    s = os.path.basename(filename)
    s = s.replace("_"," ").split(".")[0]
    return s.title()

################################################################################
# Create folders for the test
################################################################################
def make_folders(folder):
    mkdir(folder)
    mkdir(os.path.join(folder,"Mesh"))
    mkdir(os.path.join(folder,"InitialConditions"))
    mkdir(os.path.join(folder,"Output"))
    return

################################################################################
# Check if folder exists or create it
################################################################################
def mkdir(folder):
    """
    Safer mkdir that does not throws errors if folder already exists.
    """
    path = os.path.abspath(folder)
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise
    return path

################################################################################
# Saves a plain txt with matrix like entries
################################################################################
def save_data(path, filename, data, fmt="%1.8E"):
    # Load the file
    file_path = os.path.join(path, filename)
    if data is None:
        return
    if len(data.shape)==1:
        header = "%d 1" %(data.shape)
    else:
        header = "%d %d" %(data.shape)
    np.savetxt(file_path, data, fmt=fmt, header=header)
    return

################################################################################
# Prediction of values
################################################################################
def get_dL_from_dT(dT, grid_dic):
    # Compute min height
    if grid_dic["triangles"]=="rectangular":
        dt = dT / np.cos(np.pi/4)
    else: # equilateral or None
        dt = dT / np.cos(np.pi/6)
    # Compute the mean u
    u = (grid_dic["g"]*grid_dic["h_mean"])**.5 + grid_dic["u_mean"]
    # Compute dT
    dL = 4 * u * dt
    return dL

################################################################################
# Prediction of values
################################################################################
def get_dT_from_dL(dL, grid_dic):
    # Compute min height
    if grid_dic["triangles"]=="rectangular":
        dl = np.cos(np.pi/4) * dL
    else:
        dl = np.cos(np.pi/6) * dL
    # Compute the mean u
    try:
        u = (grid_dic["g"]*grid_dic["h_mean"])**.5 + grid_dic["u_mean"]
        # Compute dT
        dT = 0.25 * dl / u
    except:
        print("dT cannot be computed with provided info")
        dT = 0.1
    return dT

################################################################################
# Prediction of values
################################################################################
def get_dL_from_NElems(desired_NElems, Lx, Ly, grid_dic):
    dA = Lx*Ly/desired_NElems
    if grid_dic["triangles"]=="rectangular":
        dL = (2.*dA)**.5
    else: # equilateral or None
        dL = (2.*dA/np.cos(np.pi/6))**.5
    return dL

################################################################################
# Rectangular (Classic) grid points
################################################################################
def get_grid(xmin, xmax, ymin, ymax, grid_dic, filename, verbose=True):
    # Default value to use if options are not met
    DEFAULT_NELEMS = 5000
    # Discretization parameters
    if grid_dic["dL"] is not None:
        # Use given value of dL
        dL = grid_dic["dL"]
    else:
        NElems = grid_dic["NElems"] if (grid_dic["NElems"] is not None) else DEFAULT_NELEMS
        dL = get_dL_from_NElems(NElems, xmax-xmin, ymax-ymin, grid_dic)
    # Compute the estimation of dT, only meant to display
    if verbose:
        dT = get_dT_from_dL(dL, grid_dic)
        grid_dic["dL"] = 2*dL
        grid_dic["dT"] = dT
    else:
        dT = None
    # Select the type and de the meshing
    if grid_dic["triangles"]=="rectangular":
        return rectangular_grid(xmin, xmax, ymin, ymax, dL, dT, filename, verbose)
    else: # equilateral or None
        return equilateral_grid(xmin, xmax, ymin, ymax, dL, dT, filename, verbose)
    return

################################################################################
# Rectangular (Classic) grid points
################################################################################
def rectangular_grid(xmin, xmax, ymin, ymax, dL, dT, filename, verbose=True):
    # Domain
    nx = int( (xmax-xmin)/dL) + 1
    ny = int( (ymax-ymin)/dL) + 1
    x = np.linspace(xmin, xmax, nx)
    y = np.linspace(ymin, ymax, ny)
    dLx, dLy = x[1]-x[0], y[1]-y[0]
    X, Y = np.meshgrid(x, y, indexing='xy')
    x_mesh, y_mesh = X.flatten(), Y.flatten()
    # Print name and params
    if verbose:
        print "Test: %s" %test_name(filename)
        print "Parameters"
        print "\t [xmin, xmax] = [%.2f, %.2f], in [m]" %(xmin, xmax)
        print "\t [ymin, ymax] = [%.2f, %.2f], in [m]" %(ymin, ymax)
        print "\t dL = %.5f [m] (dL_x = %.5f, dL_y= %.5f)" %(dL, dLx, dLy)
        print "\t dT = %.5f [sec] (estimation)" %dT
    return x_mesh, y_mesh

################################################################################
# Better triangulation grid points
################################################################################
def equilateral_grid(xmin, xmax, ymin, ymax, dL, dT, filename, verbose=True):
    """
    We get the equilateral grid by superpossing two perfectly aligned grids.
    """
    dLy = dL
    dLx = dL*np.cos(np.pi/6)
    nx = int( (xmax-xmin)/(2*dLx) )
    ny = int( (ymax-ymin)/dLy )
    # Even points
    nx_even = nx+1
    ny_even = ny+1
    x1 = np.linspace(xmin, xmax, nx_even)
    y1 = np.linspace(ymin, ymax, ny_even)
    dLx = x1[1]-x1[0]
    dLy = y1[1]-y1[0]
    X, Y = np.meshgrid(x1, y1, indexing='xy')
    x1_mesh, y1_mesh = X.flatten(), Y.flatten()
    # Odd points
    nx_odd = nx
    ny_odd = ny
    x2 = np.linspace(xmin+.5*dLx, xmax-.5*dLx, nx_odd)
    y2 = np.linspace(ymin+.5*dLy, ymax-.5*dLy, ny_odd)
    X, Y = np.meshgrid(x2, y2, indexing='xy')
    x2_mesh, y2_mesh = X.flatten(), Y.flatten()
    # Top and bottom
    x_bottom = x1[:-1] + .5*dLx
    y_bottom = y1[0] + 0*x_bottom
    x_top = x1[:-1] + .5*dLx
    y_top = y1[-1] + 0*x_top
    # join the meshes
    x_mesh = np.hstack([x1_mesh,x2_mesh,x_top,x_bottom])
    y_mesh = np.hstack([y1_mesh,y2_mesh,y_top,y_bottom])
    # True mesh
    dLx = x2[0]-x1[0]
    dLy = y2[1]-y2[0]
    # Print name and params
    if verbose:
        print "Test: %s" %test_name(filename)
        print "Parameters"
        print "\t [xmin, xmax] = [%.2f, %.2f], in [m]" %(xmin, xmax)
        print "\t [ymin, ymax] = [%.2f, %.2f], in [m]" %(ymin, ymax)
        print "\t dL = %.5f [m] (dL_x = %.5f*cos(30), dL_y= %.5f)" %(dL, dLx/np.cos(np.pi/6), dLy)
        print "\t dT = %.5f [sec] (estimation)" %dT
    return x_mesh, y_mesh

################################################################################
# General Saving procedure
################################################################################
def save_test_data_on_folders(folder_path, test_values, saveBnodes):
    """
    Saves the test_values into the provided folder_path,
    using the convention taken for ANSWER.
    If "Meter2Degrees" is present and different from None,
    it converts the dictionary to be in meters and stores
    the Meter2Degrees table.
    """

    mesh_path = os.path.join(folder_path, "Mesh")
    output_path = os.path.join(folder_path, "Output")
    ic_path = os.path.join(folder_path, "InitialConditions")

    # Create the required folders
    make_folders(folder_path)

    # Convert the mesh if necesary
    #LonLatHandler.convert_dictionary_to_meter(test_values)

    # Save the Mesh
    initial_values = test_values["initial_values"]
    save_data(mesh_path, "NodeCoords.txt", initial_values["NodeCoords"])
    save_data(mesh_path, "ElemNodes.txt", initial_values["ElemNodes"], fmt="%d")
    save_data(mesh_path, "ElemNeighs.txt", initial_values["ElemNeigh"], fmt="%d")
    save_data(mesh_path, "ElemNeighSides.txt", initial_values["ElemNeighSides"], fmt="%d")
    save_data(mesh_path, "ElemSides.txt", initial_values["ElemSides"], fmt="%d")
    save_data(mesh_path, "GhostCells.txt", initial_values["GhostCells"], fmt="%d")
    # Save the table, if there anything to be saved
    if test_values.has_key("meter_to_degrees") and (test_values["meter_to_degrees"] is not None):
        m2d_path = os.path.join(mesh_path, "Optional")
        mkdir(m2d_path)
        save_data(m2d_path, "Meter2Degrees.txt", test_values["meter_to_degrees"], fmt="%f")

    # Save the Initial Conditions
    save_data(ic_path, "Bathymetry.txt", initial_values["Bj"])
    save_data(ic_path, "WaterLevel.txt", initial_values["Wj"])
    save_data(ic_path, "DischargeX.txt", initial_values["HU0j"])
    save_data(ic_path, "DischargeY.txt", initial_values["HV0j"])
    if saveBnodes:
	save_data(ic_path, "Bathymetry_at_Nodes.txt", initial_values["Bjk"])

    # Specify the execution parameters
    Output.save_execution_parameters(output_path, "ExecutionParameters.txt", test_values)

    # Specify the required output
    Output.save_time_series_configuration(output_path, "TimeSeriesConfiguration.txt", test_values["time_series"])
    Output.save_time_series_values(output_path, "TimeSeriesReferenceValues.txt", test_values["time_series"])
    Output.save_spatial_profiles_configuration(output_path, "SpatialProfilesConfiguration.txt", test_values["spatial_profile"])
    Output.save_spatial_profiles_values(output_path, "SpatialProfilesReferenceValues.txt", test_values["spatial_profile"])

    # Save readme file
    QASFormat.write_readme(folder_path)

    # Acknowledge everything is OK
    print "Files have been successfully generated.\n"

    # Print the references
    print_references(test_values["references"])

    return

################################################################################
# Print the references list
################################################################################
def print_references(ref_list):
    print "References:"
    if not ref_list:
        print "\tNo available references for this test"
    else:
        for i, ref in enumerate(ref_list):
            print "\t[%d] %s" %(i, ref)
    return

################################################################################
# Print the references list
################################################################################
def print_usage(folder, Niters=0, degrees=False):
    print("")
    # (Optional) print question usage for refining
    if Niters>0:
        print('Usage for QUESTION (if more iterations are required):')
        print('\tquestion.py --from "{0}" --refine {1:d} --to "{0}"'.format(folder, Niters))
    # Print answer usage
    print('Usage for ANSWER:')
    print('\tanswer.py "%s"' %folder)
    # Print sweet usage
    print('Usage for SWEET:')
    if degrees:
        print('\tsweet.py "%s" -stats --degrees' %folder)
        print('\tsweet.py "%s" -debug --degrees' %folder)
        print('\tsweet.py "%s" -ic --degrees' %folder)
        print('\tsweet.py "%s" -ts --degrees' %folder)
        print('\tsweet.py "%s" -sp --degrees' %folder)
    else:
        print('\tsweet.py "%s" -stats' %folder)
        print('\tsweet.py "%s" -debug' %folder)
        print('\tsweet.py "%s" -ic' %folder)
        print('\tsweet.py "%s" -ts' %folder)
        print('\tsweet.py "%s" -sp' %folder)
    return

################################################################################
# Print the references list
################################################################################
def print_sbf_usage(folder):
    print("")
    # Print sweet usage
    print('Display sbf using SWEET:')
    print('\tsweet.py "%s" -sbf' %folder)
    print('Relaunch mesh adaptor:')
    print('\tquestion.py --from_sbf {0} --to_sbf {0} --adapt 100'.format(folder))
    print('Converto to QAS format:')
    print('\tquestion.py --from_sbf "%s" --to QAS_' %folder)
    return
