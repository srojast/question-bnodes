import COMCOTFormat as COMCOT
import SBFFormat as SBF
import ArgumentHandler
import Default
import Common

import numpy as np
import sys
import os

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
################################################################################
def generate(options):
    """
    Generate SBF folder from COMCOT file.
    """

    # Only accepted arguments
    arguments = ["from_comcot", "to_sbf"]
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)
    options["to_sbf"] = options["to_sbf"].strip()
    options["from_comcot"] = options["from_comcot"].strip()

    # Required arguments
    arguments = ["from_comcot", "to_sbf"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)

    # Get the values
    comcot_path = options["from_comcot"]

    # Check the file format
    if not COMCOT.has_valid_format(comcot_path):
        print("Provided folder is not in comcot format.")
        return sys.exit(-1)

    # Load the values
    sbf_dict = COMCOT.load_as_sbf_dict(comcot_path)

    # Save in Simplified format
    to_path = options["to_sbf"]
    SBF.save(to_path, sbf_dict, convert_to_cartesian=True)

    Common.print_sbf_usage(to_path)

    return sys.exit(0)
