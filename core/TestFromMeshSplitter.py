import numpy as np
import sys

import ArgumentHandler
import MeshSplitter
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

def generate(options):
    """
    Generates the folder with answer files.
    """

    # The only valid arguments (not all simultaneously required)
    arguments = ["from_sbf", "to_sbf", "split"]
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)
    options["from_sbf"] = options["from_sbf"].strip()
    options["to_sbf"] = options["to_sbf"].strip()

    # Must have the to_folder
    arguments = ["from_sbf", "to_sbf", "split"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)

    from_folder = options["from_sbf"]
    to_folder = options["to_sbf"]
    if options['split']:
        # Everything should be ok now, so run execution
        # Run the test with the given options
        MeshSplitter.generate(from_folder, to_folder)

        # Print the usage for the benchmark
        Common.print_sbf_usage(to_folder)
        return sys.exit(0)
