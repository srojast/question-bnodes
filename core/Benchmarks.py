import Default
import Common
import Output
import Mesh

import os

if Default.DEBUG_MODE:
    from IPython import embed

def generate(folder_path, i , grid_dic, BC_list, saveBnodes):
    """
    Generates a dictionary with the labeling of the tests will be returned. They are divided into Debug, Analytical,
    Laboratory, and Field Tests.
    """
    test = {"debug":{}, "analytical":{}, "lab":{}, "field":{}, "user_defined":{}}

    #############
    # DEBUG TESTS:
    # Debug Test -1:
    I=-1
    if i==I:
        from debug_test import regular_grid_constant_depth_periodic_boundary as mytest
    test["debug"][I] = "regular_grid_constant_depth_periodic_boundary"

    # Debug Test -2:
    I -= 1
    if i==I:
        from debug_test import non_regular_grid_constant_depth_periodic_boundary as mytest
    test["debug"][I] = "non_regular_grid_constant_depth_periodic_boundary"

    # Debug Test -3:
    I -= 1
    if i==I:
        from debug_test import small_triangular_debug_domain as mytest
    test["debug"][I] = "small_triangular_debug_domain"

    # Debug Test -4:
    I -= 1
    if i==I:
        from debug_test import small_cartesian_debug_domain as mytest
    test["debug"][I] = "small_cartesian_debug_domain"

    # Debug Test -5:
    I -= 1
    if i==I:
        from debug_test import coarse_chilean_domain as mytest
    test["debug"][I] = "coarse_chilean_domain"

    ##################
    # ANALYTICAL TESTS
    I = 1
    # Analytical Test 1:
    if i==I:
        from analytical_test import lake_at_rest as mytest
    test["analytical"][I] = "lake_at_rest"

    # Analytical Test 2:
    I += 1
    if i==I:
        from analytical_test import transition_to_steady_state as mytest
    test["analytical"][I] = "transition_to_steady_state"

    I += 1
    # Analytical Test 3:
    if i==I:
        from analytical_test import propagation_of_solitary_wave as mytest
    test["analytical"][I] = "propagation_of_solitary_wave"

    I += 1
    # Analytical Test 4:
    if i==I:
        from analytical_test import dam_break as mytest
    test["analytical"][I] = "dam_break"

    I += 1
    # Analytical Test 5:
    if i==I:
        from analytical_test import hydraulic_jumps_over_constant_depth as mytest
    test["analytical"][I] = "hydraulic_jumps_over_constant_depth"

    I += 1
    # Analytical Test 6:
    if i==I:
        from analytical_test import free_oscillation_on_parabolic_basin as mytest
    test["analytical"][I] = "free_oscillation_on_parabolic_basin"

    I += 1
    # Analytical Test 7:
    if i==I:
        from analytical_test import damped_oscillation_on_parabolic_basin as mytest
    test["analytical"][I] = "damped_oscillation_on_parabolic_basin"

    I += 1
    # Analytical Test 8:
    if i==I:
        from analytical_test import run_up_of_solitary_wave_over_a_sloping_beach as mytest
    test["analytical"][I] = "run_up_of_solitary_wave_over_a_sloping_beach"

    # LABORATORY TESTS:
    # Laboratory Test 1:
    I = 20
    if i==I:
        from lab_test import solitary_wave_on_a_simple_beach as mytest
    test["lab"][I]="solitary_wave_on_a_simple_beach"

    # Laboratory Test 2:
    I += 1
    if i==I:
        from lab_test import solitary_wave_on_a_composite_beach as mytest
    test["lab"][I]="solitary_wave_on_a_composite_beach"

    # Laboratory Test 3:
    I += 1
    if i==I:
        from lab_test import solitary_wave_on_a_conical_island as mytest
    test["lab"][I]="solitary_wave_on_a_conical_island"

    # FIELD TESTS
    # Field Test 1:
    I = 40

    # Laboratory Test 4:
    if i==I:
        from lab_test import run_up_onto_a_complex_3d_beach as mytest
    test["field"][I]="run_up_onto_a_complex_3d_beach"

    # Field Test 3:
    I += 1
    if i==I:
        from field_test import concepcion_tsunami as mytest
    test["field"][I]="concepcion_tsunami"

    # Field Test 4:
    I += 1
    if i==I:
        from field_test import iquique_tsunami as mytest
    test["field"][I]="iquique_tsunami"

    # USER DEFINED TESTS :
    # ##################
    I = 100
    # Put your code here
    # User defined test 1
    """
    I += 1
    if i==I:
        from user_defined_test import gaussian_bathymetry as mytest
    test["user_defined"][I]="gaussian bathymetry created by user"
    """
    # Do not put code after this
    # ##########################

    # RETURN THE DESCRIPTION IF ASKED TO:
    all_keys = []
    [all_keys.extend(d.keys()) for d in test.values()]

    if i=="get_test_description":
        return test
    elif i in all_keys:
        folder_path = os.path.join(os.path.abspath("."), folder_path)

        # Conditional run of tests, depending on generation/downloading of data.
	if i == 41:
	    file_path = os.path.abspath(".") + "/data/Concepcion/"
            raw_test_values = mytest.get_raw_values(grid_dic, BC_list, file_path)
	    test_values = Mesh.generate(raw_test_values, saveBnodes)

	elif i == 42:
	    file_path = os.path.abspath(".") + "/data/Iquique/"
            raw_test_values = mytest.get_raw_values(grid_dic, BC_list, file_path)
	    test_values = Mesh.generate(raw_test_values, saveBnodes)

            """
            elif i == 50:
	    raw_test_values = mytest.get_raw_values(grid_dic, BC_list, folder_path)
 	    file_path = os.path.abspath(".") + "/data/Mesh/Chile_Interpolation.txt"
            test_values = MeshRefine.generate(raw_test_values,grid_dic,file_path)
            """
	else:
            # Run the test
	    raw_test_values = mytest.get_raw_values(grid_dic, BC_list)
	    test_values = Mesh.generate(raw_test_values, saveBnodes)

	Common.save_test_data_on_folders(folder_path, test_values, saveBnodes)

    else:
        print "Selected option is invalid."
    return


################################################################################
# Auxiliar Function
################################################################################
def preappend_text(text, l):
    l2 = ["%s %s"%(text, s) for s in l]
    return l2

################################################################################
################################################################################
def generate_help_string():
    """
    Generates the available options for the --test options.
    """
    # Parameters
    tests_to_be_skipped = ["debug"]
    # Generate the help
    tests_dic = generate(None, "get_test_description", None, None, None)
    # Selected the test to be generated
    help_doc = []
    all_tests = ["debug","analytical","lab","field","user_defined"]
    for test in tests_to_be_skipped:
        all_tests.remove(test)
    tests_to_be_printed = all_tests # method has taken care of
    for test_type in tests_to_be_printed:
        test_cases = tests_dic[test_type]
        aux_str = "  %s test" %test_type.upper()
        index_list = sorted(test_cases.keys())
        if test_type=="debug":
            index_list.reverse()
        for index in index_list:
            test_name = test_cases[index]
            aux_str += "\n    (%d) %s" %(index, test_name.replace("_"," ").title())
        if len(index_list)>0:
            help_doc.append(aux_str)
    return "\n".join(help_doc)

################################################################################
# This is the help_str string that will be shown on as help_str message on swept.py
################################################################################
help_str = generate_help_string()
