import numpy as np
import sys
import os

import ArgumentHandler
import Default
import Common
import Output
import Okada

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
################################################################################
def generate(options):
    """
    Overwrite the Initial Conditions using the okada surface for Wj
    and assigning (Wj, HUj, HVj) with user provided data.
    **Input**:
    **Output**:
    """

    # Arguments that are accepted for SIPAT (all others rejected)
    arguments = ["okada_surface", "from", "to"]
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)
    options["okada_surface"] = options["okada_surface"].strip()
    options["from"] = options["from"].strip()
    options["to"] = options["to"].strip()

    # No flexibily to the options, must always provide --okada_surface and --from flags

    # Get the values
    okada_file = options["okada_surface"]
    from_folder = options["from"]
    to_folder = options["to"]
    recompute_points = False

    # Check that the provided okada_file exists
    if not os.path.exists(okada_file):
        print("Provided okada file does not exists.")
        sys.exit(-1)

    # If to_folder it has been provided, we recopy the info and change W.txt
    # If to_folder has not been provided, we just overwrite the from_folder
    if to_folder:
        Common.copy(from_folder, to_folder)
        target_folder = to_folder
    else:
        target_folder = from_folder

    # Read the files
    NodeCoords = np.loadtxt(os.path.join(target_folder, "Mesh", "NodeCoords.txt"))
    ElemNodes = np.loadtxt(os.path.join(target_folder, "Mesh", "ElemNodes.txt"),
                           dtype = int)
    Meter2Degrees = np.loadtxt(os.path.join(target_folder, "Mesh", "Optional", "Meter2Degrees.txt"),
                               dtype = float)

    # Get the Water Level for the okada file
    Wj, HUj, HVj, NodeCoords, Meter2Degrees = from_Okada_file(NodeCoords, ElemNodes, Meter2Degrees,
                                                              okada_file,
                                                              recompute_points=recompute_points)

    # Overwrite W, HUj, HVj
    ic_path = os.path.join(target_folder, "InitialConditions")
    Common.save_data(ic_path, "WaterLevel.txt", Wj)
    Common.save_data(ic_path, "DischargeX.txt", HUj)
    Common.save_data(ic_path, "DischargeY.txt", HVj)

    # Save the points, if we have recomputed them
    if recompute_points:
        mesh_path = os.path.join(target_folder, "Mesh")
        m2d_path = os.path.join(target_folder, "Mesh", "Optional")
        Common.save_data(mesh_path, "NodeCoords.txt", NodeCoords)
        Common.save_data(m2d_path, "Meter2Degrees.txt", Meter2Degrees)

    # Don't reset the stored values for TS and SP.
    # We probably want to match some provided data.

    # Print the usage for the benchmark
    Common.print_usage(target_folder)
    return
