import numpy as np
from numpy import cos, sin, tan, sqrt
from numpy import log as ln
from numpy import arctan2 as atan2 # So atan2(y,x) for arctan(x/y) but with correct quadrant.
from numpy import arctan as atan   # Simply atan(x/y)
import sys

import LonLatHandler
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

TOL_DELTA_0 = 1E-12
TOL_DELTA_1 = 1E-6

################################################################################
# Version that gets called from SIPAT
################################################################################
def from_SIPAT_dict(NodeCoords, ElemNodes, Meter2Degrees,
                    subfault_dict, elastic_parameters_dict,
                    recompute_points=False):
    """
    Function that evaluates the Okada surface, using the values provided in subfault_mks_dict
    on the given points (that should be the Cell centroid coordinates).
    All arrays should be in mks.
    """
    # Repack everything
    okada_params_dict = {}
    okada_params_dict["fault_width_in_meters"] = subfault_dict["scSubW"]
    okada_params_dict["fault_length_in_meters"] = subfault_dict["scSubL"]
    okada_params_dict["fault_slip_in_meters"] = subfault_dict["scSubSlip"]
    okada_params_dict["fault_focal_depth_in_meters"] = subfault_dict["scSubDepth"]
    okada_params_dict["fault_dip_in_radians"] = subfault_dict["scSubDip"]
    okada_params_dict["fault_rake_in_radians"] = subfault_dict["scSubRake"]
    okada_params_dict["fault_strike_in_radians"] = subfault_dict["scSubStrike"]
    okada_params_dict["Mu"] = elastic_parameters_dict["mu"]
    okada_params_dict["Lambda"] = elastic_parameters_dict["lambda"]
    # Compute epicenter in meters using the Meter2Degrees table
    Lon_epi = LonLatHandler.longitude_to_range(subfault_dict['scSubLon'])
    Lat_epi = subfault_dict['scSubLat']
    LonLat_epicenter = np.array([[Lon_epi, Lat_epi]])
    meter_epicenter = LonLatHandler.convert_array_to_meter(LonLat_epicenter, Meter2Degrees)
    if np.isnan(meter_epicenter).any():
        print("Epicenter outside computed domain for SIPAT simulation.")
        print("Epicenter Longitude : {0}".format(Lon_epi))
        print("Epicenter Latitude : {0}".format(Lat_epi))
        print("\tSkipping subfault.")
        return None
    x_epi, y_epi = meter_epicenter[0,0], meter_epicenter[0,1]
    # Compute the Initial Conditions
    Wj, HUj, HVj = InitialConditions(NodeCoords, ElemNodes, x_epi, y_epi, okada_params_dict)
    # Return computed values
    if recompute_points:
        print("Recompute points not implemented yet")
        return Wj, HUj, HVj, NodeCoords, ElemNodes
    else:
        return Wj, HUj, HVj

################################################################################
################################################################################
def InitialConditions(NodeCoords, ElemNodes, x_epi, y_epi, okada_mks_params):
    """
    Given the NodeCoordinates, the Element Connectivity, the Meter2Degrees table, and
    the parameters provided in the okada_mks_params dictionary, we create the initial
    surface and discharges.
    **Input**:
    **Output**:
    """
    # We'll evaluate okada in the centroid of cells
    xj, yj = Common.get_centroids(NodeCoords, ElemNodes)
    # Create the Initial surface
    Wj = UnshiftedModel(xj, yj, x_epi, y_epi, okada_mks_params)
    HUj = np.zeros(Wj.shape)
    HVj = np.zeros(Wj.shape)
    return Wj, HUj, HVj

################################################################################
################################################################################
def UnshiftedModel(x_mesh, y_mesh, x_epi, y_epi, mks_params):
    """
    Returns the deformation at position (x,y) using Okada' model.
    x_global, y_global are in global coordinates in meters, with >0 for North/East.
    mks_params is a dict where all the variables have been transformed to mks
    (rads, meters, secs, etc.).
    """
    print("\tShifting variables before applying Okada's model")
    # Width, lenght and depth
    W  = mks_params['fault_width_in_meters']
    L  = mks_params['fault_length_in_meters']
    depth = mks_params['fault_focal_depth_in_meters']
    slip = mks_params['fault_slip_in_meters']
    # Angles
    dip_r = mks_params['fault_dip_in_radians']       # angle delta
    rake_r = mks_params['fault_rake_in_radians']     #
    strike_r = mks_params['fault_strike_in_radians'] # angle theta, 0-2pi, relative to north
    # Parameters
    Mu = mks_params['Mu']
    Lambda = mks_params['Lambda']
    # We take half width and half length to easily shift the model
    hw = 0.5 * W * cos(dip_r)
    hl = 0.5 * L
    # Distance local to the corner in x,y plane
    lx = x_mesh - x_epi + hl*sin(strike_r) - hw*cos(strike_r)
    ly = y_mesh - y_epi + hl*cos(strike_r) + hw*sin(strike_r)
    # change to (u,v) aligned as in okada model
    # Fix from here
    x_shifted_and_aligned_to_L = + lx * sin(strike_r) + ly * cos(strike_r)
    y_shifted_and_aligned_to_W = - lx * cos(strike_r) + ly * sin(strike_r)
    # Compute all the variables required for the okada (raw) model
    U1 = cos(rake_r)*slip;
    U2 = sin(rake_r)*slip;
    U3 = 0.0
    d  = depth + 0.5 * W * sin(dip_r)
    # There is no need to shift the model back
    # Compute the okada model
    displacement = okada_raw_model(x_shifted_and_aligned_to_L,
                                   y_shifted_and_aligned_to_W,
                                   U1, U2, U3,
                                   L, W, d,
                                   dip_r, Mu, Lambda)
    # Should we interpolate to get a better Okada surface??
    #print("***Think about dX, dY, dZ in Okada's Model")
    return displacement[:,2] # Only in z by now

################################################################################
#
################################################################################
def okada_raw_model(x, y, U1, U2, U3, L, W, d, delta, Mu, Lambda):
    """
    Returns the deformation (ux, uy, uz) at position (x,y) using Okada' model,
    in Okada's notation.
    **Input**:
        x, y are in meters, aligned with the fault sides,
        x being the strike and lenght (L) side, y being the with (W) side
    **Output**:
        array of dimensions (len(x), 3) with the displacement in x, y and z
    """
    print("\tComputing surface using Okada's model")
    # Compute local coordinates
    p = y*cos(delta) + d*sin(delta)
    arguments = [x, y, U1, U2, U3, d, delta, Mu, Lambda] # For easiness
    # Compute the strike slip
    u_strike = Chinnery(strike_slip_displacement_function, x, p, L, W, *arguments)
    # Compute the dip slip
    u_dip    = Chinnery(dip_slip_displacement_function, x, p, L, W, *arguments)
    # Compute the tensile fault
    u_tens   = Chinnery(tensile_fault_displacement_function, x, p, L, W, *arguments)
    return (u_strike + u_dip + u_tens)

################################################################################
################################################################################
def Chinnery(f, x, p, L, W, *args):
    """
    Chinnery Notation for easier evaluation.
    **Inputs**: Function f, "origin" points x and p, and fault dimensions L and W.
                The *args are additional arguments required for the function f.
    """
    return f(x,p,*args) - f(x,p-W,*args) - f(x-L,p,*args) + f(x-L,p-W,*args)

################################################################################
################################################################################
def strike_slip_displacement_function(xi, eta,
                                      x, y, U1, U2, U3, d,
                                      delta, Mu, Lambda):
    """
    Compute the strike slip displacement function, using Okada's 95 model,
    for a generic (xi, eta) position. Chinery function must still be applied.
    """
    I1, I2, I3, I4, I5 = AuxiliarParameters(xi, eta, x, y, d, delta, Mu, Lambda)
    p, q, ybar, dbar, R, X = LocalNotation(xi, eta, x, y, d, delta)
    atan_eval = atan_stable(xi*eta, q*R) # arctan((xi*eta)/(q*R))
    displacement = np.zeros((len(x), 3))
    displacement[:,0] = (xi*q)/(R*(R+eta)) + atan_eval + I1*sin(delta)
    displacement[:,1] = (ybar*q)/(R*(R+eta)) + (q*cos(delta))/(R+eta) + I2*sin(delta)
    displacement[:,2] = (dbar*q)/(R*(R+eta)) + (q*sin(delta))/(R+eta) + I4*sin(delta)
    return (-0.5 * U1 / np.pi) * displacement

################################################################################
################################################################################
def dip_slip_displacement_function(xi, eta,
                                      x, y, U1, U2, U3, d,
                                      delta, Mu, Lambda):
    """
    Compute the dip slip displacement function, using Okada's 95 model,
    for a generic (xi, eta) position. Chinery function must still be applied.
    """
    I1, I2, I3, I4, I5 = AuxiliarParameters(xi, eta, x, y, d, delta, Mu, Lambda)
    p, q, ybar, dbar, R, X = LocalNotation(xi, eta, x, y, d, delta)
    displacement = np.zeros((len(x), 3))
    atan_eval = atan_stable(xi*eta, q*R)
    displacement[:,0] = q/R - I3*sin(delta)*cos(delta)
    displacement[:,1] = (ybar*q)/(R*(R+xi)) + cos(delta)*atan_eval - I1*sin(delta)*cos(delta)
    displacement[:,2] = (dbar*q)/(R*(R+xi)) + sin(delta)*atan_eval - I5*sin(delta)*cos(delta)
    return (-0.5 * U2 / np.pi) * displacement

################################################################################
################################################################################
def tensile_fault_displacement_function(xi, eta,
                                        x, y, U1, U2, U3, d,
                                        delta, Mu, Lambda):
    """
    Compute the tensile fault displacement function, using Okada's 95 model,
    for a generic (xi, eta) position. Chinery function must still be applied.
    """
    I1, I2, I3, I4, I5 = AuxiliarParameters(xi, eta, x, y, d, delta, Mu, Lambda)
    p, q, ybar, dbar, R, X = LocalNotation(xi, eta, x, y, d, delta)
    atan_eval = atan_stable(xi*eta, q*R)
    displacement = np.zeros((len(x), 3))
    displacement[:,0] = (q*q)/(R*(R+eta)) - I3*sin(delta)**2
    displacement[:,1] = (-dbar*q)/(R*(R+xi)) - sin(delta)*( (xi*q)/(R*(R+eta)) - atan_eval ) - I1*sin(delta)**2
    displacement[:,2] = (+ybar*q)/(R*(R+xi)) + cos(delta)*( (xi*q)/(R*(R+eta)) - atan_eval ) - I5*sin(delta)**2
    return (+0.5 * U3 / np.pi) * displacement

################################################################################
################################################################################
def AuxiliarParameters(xi, eta, x, y, d, delta, Mu, Lambda):
    """
    """
    # values in Chinery notation
    p, q, ybar, dbar, R, X = LocalNotation(xi, eta, x, y, d, delta)
    # Auxiliar elastic constant
    c_elast = Mu/(Lambda+Mu) # =(1-2*nu)
    # Compute the Auxiliar parameters in order I5, I4, I3, I1
    sin_delta, cos_delta = sin_cos_stable(delta)

    # Conditional Evaluation
    if np.abs(np.cos(delta))<TOL_DELTA_0:
        # I5 -> I1
        I5 =     - c_elast * ( xi / (R + dbar)) * sin_delta
        I1 = -.5 * c_elast * ( xi / (R + dbar) ) * ( q / (R + dbar) )
        # I4 -> I3 -> I2
        I4 =     - c_elast * q / (R + dbar)
        I3 = +.5 * c_elast * ( eta/(R+dbar) + ybar * q / (R+dbar)**2 - ln(R+eta) )
        I2 = - c_elast * ln(R+eta) - I3
    else:
        # I5 -> I1
        #num5 = eta * (X + q*cos_delta) + X*(R+X)*sin_delta
        #den5 = xi * (R+X) * cos_delta
        num5 = eta * ((X + q*cos_delta)/(R+X)) + X*sin_delta
        den5 = xi * cos_delta
        I5 = +c_elast * 2. * atan_stable(num5, den5)# / cos_delta
        I1 = -(c_elast * xi / (R + dbar) + sin_delta*I5) / cos_delta
        # I4 -> I3 -> I2
        I4 = +c_elast * ( ln(R+dbar) - sin_delta * ln(R+eta)) / cos_delta
        I3 =(+c_elast * ( ybar/(R+dbar) + ln(R+eta)*cos_delta) + I4*sin_delta ) / cos_delta
        I2 = -c_elast * ln(R+eta) - I3

    # Return them
    return I1, I2, I3, I4, I5

################################################################################
################################################################################
def LocalNotation(xi, eta, x, y, d, angle):
    """
    Computes the local notation used to eval all formulas.
    angle is the delta in Okada's model (changed to avoid confusion)
    """
    sin_angle, cos_angle = sin_cos_stable(angle)
    p = y*cos_angle + d*sin_angle
    q = y*sin_angle - d*cos_angle
    ybar = eta*cos_angle + q*sin_angle
    dbar = eta*sin_angle - q*cos_angle
    R = sqrt(xi**2 + eta**2 + q**2)
    X = sqrt(xi**2 + q**2)
    return p, q, ybar, dbar, R, X

################################################################################
# Stable versions of sin, cos and atan
################################################################################
def atan_stable(num, den):
    """
    Uses atan2 to return the correct value, as in atan but with x=0 available
    """
    den_sign = np.where(den<0.0, -1, +1) # Works better than np.sign(num), as it doesn't have sign=0
    atan_aux = atan2(den_sign*num, den_sign*den)
    return atan_aux

def sin_cos_stable(angle, use_stable=False):
    """
    Linearized version
    """
    if np.abs(angle-np.pi/2) < TOL_DELTA_1: # angle close to pi/2
        epsilon = angle - 0.5*np.pi
        cos_angle = -epsilon
        sin_angle = 1.- .5*epsilon**2
        #print "+:", cos_angle - cos(angle)
        #print "+:", cos_angle - cos(angle)
    if np.abs(angle+np.pi/2) < TOL_DELTA_1: # angle close to -pi/2
        epsilon = angle + 0.5*np.pi
        cos_angle = +epsilon
        sin_angle = -1.+ .5*epsilon**2
    else:
        cos_angle = cos(angle)
        sin_angle = sin(angle)
    # Undo
    if use_stable==False:
        cos_angle = cos(angle)
        sin_angle = sin(angle)
    return sin_angle, cos_angle


################################################################################
# Version that gets called direcly from OKADA
################################################################################
def from_Okada_file(NodeCoords, ElemNodes, Meter2Degrees,
                    okada_file,
                    recompute_points=False):
    """
    Function that evaluates the Okada surface, using the values provided in subfault_mks_dict
    on the given points (that should be the Cell centroid coordinates).
    All arrays should be in mks.
    """
    # Produce a unique dict with all the required values
    # Read the OkadaFile
    deg_params = load_okada_params(okada_file)

    # Get Longitude and Latitude in meters
    Lon_epi = LonLatHandler.longitude_to_range(deg_params["EpicenterLongitude"])
    Lat_epi = deg_params["EpicenterLatitude"]
    LonLat_epicenter = np.array([[Lon_epi, Lat_epi]])
    meter_epicenter = Common.convert_array_to_meter(LonLat_epicenter, Meter2Degrees)
    x_epi, y_epi = meter_epicenter[0,0], meter_epicenter[0,1]

    # Create the parameter for the evaluation
    deg2rad = np.pi/180.
    mks_params = {
                  "DipAngle": deg_params["DipAngle"]*deg2rad,
                  "SlipAngle": deg_params["SlipAngle"]*deg2rad,
                  "DipAngle": deg_params["DipAngle"]*deg2rad,
                  "Dislocation": deg_params["Dislocation"],
                  "FaultLength": deg_params["FaultLength"],
                  "FaultWidth": deg_params["FaultWidth"],
                  "FocalDepth": deg_params["FocalDepth"],
                  }

    # Compute Surface
    Wj = np.zeros(ElemNodes.shape[0])
    HUj = Wj
    HVj = Wj

    # Recompute the points

    # Return computed values
    return Wj, HUj, HVj, NodeCoords, Meter2Degrees

################################################################################
################################################################################
def load_okada_params(file_path):
    """
    Returns an dict with the okada params
    **Input**:
    **Output**:
    """
    dic = {}
    keys = ['DipAngle', 'Dislocation',
            'EpicenterLatitude', 'EpicenterLongitude',
            'FaultLength', 'FaultWidth',
            'FocalDepth', 'SlipAngle',
            'StrikeDirection']
    with open(file_path, "r") as fh:
        for line in fh.readlines():
            for key in keys:
                if key.lower() in line.lower().replace(" ",""):
                    dic[key] = float(line.split(":")[-1])
    if len(keys)!=len(dic.keys()):
          print keys
          print sorted(dic.keys())
          print "Error: Not all required keys have been provided in file"
          return {}
    # Angles are in degrees
    return dic
