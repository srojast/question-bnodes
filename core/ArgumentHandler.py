import Default

import sys
import os

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# General handler (used in all "TestFrom"-like files)
################################################################################
def file_exists(file_path):
    """
    Returns boolean indicating if provided file exists
    **Input**: path string
    **Output**: boolean
    """
    if os.path.isfile(file_path) and os.path.getsize(file_path)>0:
        return True
    print("Provided file {0} does not exists".format(file_path))
    return False

################################################################################
# General handler (used in all "TestFrom"-like files)
################################################################################
def accepted_arguments(options, arguments):
    """
    Returns boolean indicating if used arguments are accepted arguments.
    **Input**: dict of options arguments from argparse library and valid arguments.
    **Output**: boolean
    """
    for argument, value in options.items():
        if (value is not None) and argument not in arguments:
            print("flag --{0} is NOT applicable".format(argument))
            return False
    return True

################################################################################
# General handler (used in all "TestFrom"-like files)
################################################################################
def required_arguments(options, arguments):
    """
    Returns boolean indicating if the required arguments are present
    **Input**: dict of options arguments from argparse library.
    **Output**: boolean
    """
    # Checks that only some combination of arguments is valid
    for argument in arguments:
        if options[argument] is None:
            print("flag --{0} is required".format(argument))
            return False
    return True

################################################################################
# General handler (used in some "TestFrom"-like files)
################################################################################
def at_most_one_argument(options, valid_arguments):
    """
    Returns boolean indicating if more than one argument has been selected
    **Input**: dict of options arguments from argparse library.
    **Output**: boolean
    """
    # Checks that only some combination of arguments is valid
    checked_options = [int(options[k] is not None) for k in valid_arguments]
    if sum(checked_options)>1:
        tests_str = "--" + ", --".join(valid_arguments)
        print("You can select at most one option among the following:\n\t{0}".format(tests_str))
        return False
    return True

################################################################################
# General handler (used in some "TestFrom"-like files)
################################################################################
def only_one_argument(options, valid_arguments):
    """
    Returns boolean indicating if only one argument has been selected
    **Input**: dict of options arguments from argparse library.
    **Output**: boolean
    """
    # Checks that only some combination of arguments is valid
    checked_options = [int(options[k] is not None) for k in valid_arguments]
    if sum(checked_options)!=1:
        tests_str = "--" + ", --".join(valid_arguments)
        print("You can select only one option among the following:\n\t{0}".format(tests_str))
        return False
    return True

###############################################################################
# Specific argument handler for --benchmark
################################################################################
def BC_from_options(options):
    """
    Convenient wrapper for obtaining a unique  BC list.
    Regardless of providing "BC" or "BC_list", we always produce a BC_list.
    **Input**: dict with options, has by construction the keys "BC_list" and "BC".
    **Output**: list with 4 strings (BC_list).
    """
    if options["BC_list"] is not None:
        BC_list = [BCi.lower() for BCi in options["BC_list"]]
        # If BC_list is provided, we use it
        return BC_list
    else:
        # If BC_list is not provided, we check the values for BC
        if options["BC"] is None:
            # If BC is not provided, we just return None everywhere
            return None
        else:
            return [options["BC"],]*4

################################################################################
# Specific argument handler for --benchmark
################################################################################
def valid_BC_list(BC_list):
    """
    Checks if provided Boundary Conditions are valid.
    **Input**: list of 4 strings.
    **Output**: boolean indicating if the provided BC_list is valid.
    """
    BC_choices = Default.ImplementedBoundaryConditions()

    # By construction, None is a valid BC_list
    if BC_list == None:
        return True
    # All Boundary conditions must be in the available ones
    for BCi in BC_list:
        if BCi not in BC_choices:
            print "The BC '%s' is not valid. Choose BC among %s." %(BCi, ", ".join(BC_choices))
            return False
    # Check periodicity
    if BC_list[0]=="periodic" and BC_list[1]!="periodic":
        print "Must provide matching Periodic BC"
        return False
    if BC_list[1]=="periodic" and BC_list[0]!="periodic":
        print "Must provide matching Periodic BC"
        return False
    if BC_list[2]=="periodic" and BC_list[3]!="periodic":
        print "Must provide matching Periodic BC"
        return False
    if BC_list[3]=="periodic" and BC_list[2]!="periodic":
        print "Must provide matching Periodic BC."
        return False
    # Else, they are well defined
    return True

################################################################################
# Specific argument handler for --benchmark
################################################################################
def benchmark_grid_from_options(options):
    """
    Creates a dict with the grid options provided for the user and filled with
    default values.
    **Input**: options dictionary created with the argparse library.
    **Output**: specific grid dictionary with flags required for meshing.
    """
    # Set the grid options dict
    grid_dic  = {}
    grid_dic["NElems"] = options["NElems"]
    grid_dic["triangles"] = options["triangles"]
    grid_dic["dL"] = options["dL"]
    return grid_dic


################################################################################
# Specific argument handler for --flat, --bell or --wave
################################################################################
def custom_water_level_dic_from_options(options):
    """
    Options for Water Level
    Saves the custom water level and the parameters,
    so it can generated and override the provided (W,HU,HV) for the test.
    **Input**: options dictionary created with the argparse library.
    **Output**: specific dictionary with type of custom water level and
                provided parameters.
    """
    custom_water_level = {}
    if options["flat"]:
        custom_water_level["type"] = "flat"
        custom_water_level["parameters"] = options["flat"]
    elif options["bell"]:
        custom_water_level["type"] = "bell"
        custom_water_level["parameters"] = options["bell"]
    elif options["wave"]:
        custom_water_level["type"] = "wave"
        custom_water_level["parameters"] = options["wave"]
    else:
        custom_water_level["type"] = None
        custom_water_level["parameters"] = None
    return custom_water_level
