from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import NearestNDInterpolator
from scipy.interpolate import griddata
import numpy as np
import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# LINEAR INTERPOLATION FUNCTION
################################################################################
class ReusableInterpolator:
    """An interpolator class."""
    def __init__(self,points,values=None):
        if values==None:
            x,y,z=points
        else:
            x,y=points
            z=values
        self.linear=LinearNDInterpolator((x,y),z)
        self.nearest=NearestNDInterpolator((x,y),z)
    def __enter__(self):
        return self
    def interpolate(self,points):
        lininterp = self.linear(points)
        nans = np.where(np.isnan(lininterp) == True)
        if nans[0].size>0:
            nearextrap = self.nearest(points[:,0][nans],points[:,1][nans])
            lininterp[nans] = nearextrap
        return lininterp

def interpolate2D(new_mesh, old_mesh, old_values):
    """
    Wraps scipy's griddata interpolation for convenience.
    If interpolation methods needs to change must change only here.
    new_mesh:  [ [x'_1,...,x'_n],[y'_1,...,y'_n]]
    old_mesh:  [ [x_1,...,x_n],[y_1,...,y_n]]
    old_values:  [z_1,...,z_n]
    """
    old_is_1D = ( len(old_values.T)==1 )
    NewMeshPoints = new_mesh.shape[1]
    if old_is_1D:
        # Case where old_mesh is just a float
        new_z = old_mesh * np.ones(NewMeshPoints)
    else:
        old_xy = old_mesh
        new_xy = new_mesh
        old_z = old_values
        new_z = griddata(old_xy, old_z, new_xy, method='linear') # cubic shows error in boundaries

    nans=np.isnan(new_z)
    if nans.size>0:
        nearest=griddata(old_xy, old_z, new_xy[nans], method='nearest')
        new_z[nans]=nearest

    return new_z.flatten()

################################################################################
# AUXILIAR FUNCTION TO FIX THE DELAUNAY
################################################################################
def get_mask_for_true_domain(tri, domain_points, exclude_points):
    x,y = tri.points.T
    centroids = tri.points[tri.vertices].mean(axis=1)
    d = compute_distance(centroids, domain_points, exclude_points)
    mask = d<0 # The ones in the exterior are positive
    return mask

def get_mask_for_irregular_cells(tri):
    """
    Returns a mask with the border elements that have a abnormal shape,
    measured with the triangle quality
    """
    q, _ = compute_triangle_quality(tri)
    neighs = tri.neighbors.copy()
    border_cells = neighs.min(axis=1)<0
    delete_cells = np.logical_and(border_cells, q<0.20)
    return delete_cells

def fix_delaunay(tri, delete_mask):
    """
    We fix the delaunay by deleting the unwanted cells.
    This can be achieved removing the unwanted triangles (simplices)
    and the neighbors.
    """
    select_mask = np.logical_not(delete_mask)
    # Skip if all cells must be kept
    if select_mask.all():
        return
    # size of the wrong_to_right_array; +1 to have an extra place for -1
    N = len(select_mask) + 1
    # Fixing the simplices
    tri.nsimplex = select_mask.sum()
    tri.simplices = tri.simplices[select_mask]
    tri.equations = tri.equations[select_mask,:]
    # Fixing the neighbors
    aux = np.zeros(N, dtype=int)
    aux[delete_mask] = -1
    wrong_to_right_array = np.arange(N) + aux.cumsum()
    wrong_to_right_array[delete_mask] = -1
    wrong_to_right_array[-1] = -1
    # Now subselect and remove all reference to old neigbors
    tri.neighbors = tri.neighbors[select_mask]
    for i in xrange(len(tri.neighbors)):
        tri.neighbors[i,:] = wrong_to_right_array[tri.neighbors[i,:]]
    return

################################################################################
# Triangle Quality
################################################################################
def compute_triangle_quality(tri):
    """
    Computes triangle quality using the same formula as in Matlab's pdetriq.
    """
    ljk = get_element_edge_lengths(tri.simplices.T, tri.points.T)
    Tj = get_element_areas(ljk)
    q = 4.*Tj*np.sqrt(3.) / (np.power(ljk,2).sum(axis=0))
    l_mean = np.sqrt(4*Tj/np.sqrt(3))
    n_smaller_side = (ljk < l_mean).sum(axis=0)
    worst_min_side_node = ljk.argmin(axis=0)
    worst_max_side_node = 2 - ljk.argmax(axis=0)
    worst_local_node = np.where(n_smaller_side==1, worst_min_side_node, worst_max_side_node)
    worst_node = 3*np.arange(len(worst_local_node)) + worst_local_node
    return q, tri.simplices.flatten()[worst_node]

def get_element_edge_lengths(Elem, mesh_points):
  """
  Gets the element edge sides and returns in order
  Inputs   :
    Elem   : Conectivity array of Elements, 	   [3, nElem]
    Points : Node coordinates array of each node,  [2, nNode]
  Outputs  :
    ljk    : Side lengths for each triangle,       [3, nElem]
  """
  # Small wrapper for DRYing the code
  def length(Elem, mesh_poins, i, j):
    u = mesh_points[:,Elem[i,:]] - mesh_points[:,Elem[j,:]]
    return np.sqrt(u[0,:]**2 + u[1,:]**2)

  #Tangent Vector side: (12)
  L1 = length(Elem, mesh_points, 0, 1)

  #Tangent Vector side: (12)
  L2 = length(Elem, mesh_points, 1, 2)

  #Tangent Vector side: (12)
  L3 = length(Elem, mesh_points, 2, 0)

  ljk = np.array([L1,L2,L3])

  return ljk

def get_element_areas(eel):
  """
  Computes the area for each triangle using Heron's formula.
  eel stands for element edge length
  Inputs   :
    ljk    : Side lengths for each triangle,       [3, nElem]
  Outputs  :
    Tj     : Area of the j-th Triangle,            [1, nElem]
  """
  #Semi-perimeter of each triangle:
  s = 0.5 * eel.sum(axis=0)

  #Heron's formula for area:
  A2 = s * (s-eel[0,:]) * (s-eel[1,:]) * (s-eel[2,:])

  return np.sqrt(A2)

################################################################################
# COMPUTE DISTANCES WITH POINTS AND EXCLUDE POINTS
################################################################################
def get_linear_coefficients(bbox_points):
    """
    Returns the coefficients for the line joining the points.
    Points (x,y) are assumed to be ordered to create a polygon anticlockwise.
    """
    NPoints = bbox_points.shape[0]
    coeffs = np.zeros([NPoints,3])
    for i in range(NPoints):
        j = (i+1)%NPoints
        xi, yi = bbox_points[i,:]
        xj, yj = bbox_points[j,:]
        a = yj-yi
        b = xi-xj
        c = -xi*yj + xj*yi
        n = float(np.sqrt(a**2+b**2))
        coeffs[i,:] = [a/n, b/n, c/n]
    return coeffs

def compute_distance_for_bbox(points, bbox_points):
    """
    """
    linear_coefficients = get_linear_coefficients(bbox_points)
    NPoints = points.shape[0]
    points_aux = np.ones([3, NPoints])
    points_aux[:2,:] = points[:,:2].T
    d_matrix = np.dot(linear_coefficients, points_aux)
    return d_matrix.max(axis=0)

def compute_distance(points, bbox_points, remove_polygon):
    """
    """
    d_in = compute_distance_for_bbox(points, bbox_points)
    if remove_polygon is not None:
        d_out = compute_distance_for_bbox(points, remove_polygon)
        return np.maximum(d_in, -d_out)
    else:
        return d_in

def get_fix_points(bbox_points, remove_polygon):
    """
    Returs the fix points (corners) of the polygon.
    """
    if remove_polygon is None:
        return bbox_points
    # See wich bbox points are inside the exclude bbox
    d = compute_distance_for_bbox(bbox_points, remove_polygon)
    bbox_out_exclude_bbox = [di>0 for di in d]
    d = compute_distance_for_bbox(remove_polygon, bbox_points)
    exclude_bbox_in_bbox = [di<0 for di in d]
    # Append the trivial cases
    fix_points = []
    for i in range(bbox_points.shape[0]):
        if bbox_out_exclude_bbox[i]:
            fix_points.append(bbox_points[i])
    for j in range(remove_polygon.shape[0]):
        if exclude_bbox_in_bbox[j]:
            fix_points.append(remove_polygon[j])
    # Append the complex cases
    if all(bbox_out_exclude_bbox):
        # No intersections to be computed
        return np.array(fix_points)
    else:
        # Compute the first intersection
        bbox_index1 = get_first_pattern(bbox_out_exclude_bbox, False, True)
        bbox_index2 = (bbox_index1+1)%len(bbox_out_exclude_bbox)
        bbox_index_array = np.array([bbox_index1, bbox_index2])
        points_1 = bbox_points[bbox_index_array, :]
        exclude_bbox_index1 = get_first_pattern(exclude_bbox_in_bbox, False, True)
        exclude_bbox_index2 = (exclude_bbox_index1 +1) % len(exclude_bbox_in_bbox)
        exclude_bbox_index_array = np.array([exclude_bbox_index1, exclude_bbox_index2])
        points_2 = remove_polygon[exclude_bbox_index_array, :]
        fix_points.append(compute_intersection(points_1, points_2))
        # Compute the second  intersection
        bbox_index1 = get_first_pattern(bbox_out_exclude_bbox, True, False)
        bbox_index2 = (bbox_index1+1)%len(bbox_out_exclude_bbox)
        bbox_index_array = np.array([bbox_index1, bbox_index2])
        points_1 = bbox_points[bbox_index_array, :]
        exclude_bbox_index1 = get_first_pattern(exclude_bbox_in_bbox, True, False)
        exclude_bbox_index2 = (exclude_bbox_index1 +1) % len(exclude_bbox_in_bbox)
        exclude_bbox_index_array = np.array([exclude_bbox_index1, exclude_bbox_index2])
        points_2 = remove_polygon[exclude_bbox_index_array, :]
        fix_points.append(compute_intersection(points_1, points_2))
        # return the fix points
        return np.array(fix_points)

def get_first_pattern(mask, b1, b2):
    """
    Returns the index for the fist in and the first out
    """
    N = len(mask)
    for i in range(N):
        if mask[i]==b1 and mask[(i+1)%N]==b2:
            return i
    print("Pattern not found")
    return None

def compute_intersection(points_1, points_2):
    """
    Computes the intersection of the two pais of points
    """
    x1, y1 = points_1[0,:]
    x2, y2 = points_1[1,:]
    u1, v1 = points_2[0,:]
    u2, v2 = points_2[1,:]
    A = np.array([[x2-x1,u1-u2],[y2-y1,v1-v2]])
    b = np.array([[u1-x1],[v1-y1]])
    alpha, beta = np.linalg.solve(A,b).flatten()
    if (not 0<=alpha<=1) or (not 0<=beta<=1):
        print("Check, something fishy here")
        return None
    else:
        return points_1[0,:] + alpha * (points_1[1,:]-points_1[0,:])
