from scipy.interpolate import griddata
import numpy as np
import pickle
import glob
import sys
import os

import LonLatHandler
import QASFormat
import Default
import Common
import Mesh

if Default.DEBUG_MODE:
    from IPython import embed

'''
WARNING: Do not use this library directly. Use SBFFormat instead.
'''

################################################################################
# LOAD DATA IN SIMPLIFIED FORMAT
################################################################################
def load_as_sbf_dict(from_path):
    """
    Returns the sbf dictionary with the data available in the provided folder.
    """
    print("Reading files in simplified format.")
    sbf_dict = {}
    # Bathymetry
    filename = os.path.join(from_path, "Bathymetry.txt")
    if os.path.exists(filename):
        print("\tLoading Bathymetry file.")
        mesh_and_B = np.loadtxt(filename, ndmin=2)
        sbf_dict["Mesh"] = mesh_and_B[:,:2]
        sbf_dict["Bathymetry"] = mesh_and_B[:,2]
    else:
        print("\t!!! Bathymetry.txt file not found!")
        sys.exit(-1)

    # Coordinate System
    filename = os.path.join(from_path, "CoordinateSystem.txt")
    if os.path.exists(filename):
        print("\tLoading Coordinate System file.")
        CS, LP = LonLatHandler.load_coordinate_system(filename)
        sbf_dict["CoordinateSystem"] = CS
        sbf_dict["LinearizationPoint"] = LP
    else:
        print("\t!!! CoordinateSystem.txt file not found!")
        sys.exit(-1)

    # Time Series Buoy Coordinates
    filename = os.path.join(from_path, "TimeSeriesBuoyCoordinates.txt")
    if os.path.exists(filename):
        print("\tLoading Time Series Buoy Coordinates.")
        TS = np.loadtxt(filename, ndmin=2)
        if TS.shape[0]>0:
            sbf_dict["TimeSeriesBuoyCoordinates"] = TS
    else:
        print("\t\tAssuming no Time Series Buoys")

    # Spatial Profile Buoy Coordinates
    filename = os.path.join(from_path, "SpatialProfileBuoyCoordinates.txt")
    if os.path.exists(filename):
        print("\tLoading Spatial Profile Buoy Coordinates.")
        SP = np.loadtxt(filename, ndmin=2)
        if SP.shape[0]>1:
            sbf_dict["SpatialProfileBuoyCoordinates"]
    else:
        print("\t\tAssuming no Spatial Profile Buoys")

    return sbf_dict
