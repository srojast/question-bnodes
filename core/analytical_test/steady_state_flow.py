import numpy as np

from .. import Default
from .. import Common

def water_level_solution(x, y, t, xo, yo, ho, sigma):
    """
    Computes the analytical time serie solution for the steady state flow
    """
    r  = np.sqrt((x - xo)**2 + (y - yo)**2)
    B  = 0.5 * np.exp(-(r/sigma)**2)
    w  = np.maximum(B,ho) + 0.0 * t
    return w

def get_raw_values(grid_dic, terminal_BCs):
    """
    DESCRIPTION:
    This analytical case attempts to study the the well-balanced property over a variable topography with dry zones. The objective of 
    the model is to test the ability to preserve steady state.
    """
    # References
    references = ["Thacker W., 'Some exact solution to the nonlinear shallow water equations'. Journal of Fluid Mechanics (107) 499" +
  		  "-508, 1981", "A. Brodtkorb, M. Saetra, M. Altinakar, Efficient shallow water initial_valuess on GPUs: Implementation, " + 
  		  "visualization, verification, and validation, Computers & Fluids (55) 1-12, 2012"] 

    # Suggested parameter usage
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params =  {"Tmax":75.0, "g":Default.g(), "manning":0.0, "tol_dry":Default.tol_dry()}

    ################################################################################
    # MODEL PARAMETERS
    ################################################################################
    xo = 5.00             # [m]
    yo = 5.00             # [m]
    ho = 0.30             # [m]
    g  = exec_params["g"] # [m/s2]

    # Discretization parameters
    xmin = 0.00   # [m]
    xmax = 10.0   # [m]
    ymin = 0.00   # [m]
    ymax = 10.0   # [m]

    # Related variables
    sigma =  0.5  # [m]
  
    ################################################################################
    # INITIAL CONDITIONS AND BATHYMETRY
    ################################################################################
    # Mesh
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = ho
    grid_dic["u_mean"] = 0.0

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    # Bathymetry: (B)
    r  = np.sqrt((x - xo)**2 + (y - yo)**2)
    B  = 0.5 * np.exp(-(r/sigma)**2)
    B  = np.array([x, y, B]).T

    # Water Level Surface: (W)
    W  = ho + 0.0 * r
    W  = np.array([x, y, W]).T

    # Flux Velocities: (U0,V0)
    U0 = np.array([0.0])
    V0 = np.array([0.0])
    
    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}

    ################################################################################
    # OUTPUT
    ################################################################################
    # Movie Animation Step
    movie = {"compute":True, "dt_save":20.0}

    # inundation map, heat map and arrival times maps
    inundation = {"compute":True}
    heatmap = {"compute":True}
    arrival_time = {"compute":True,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Config
    TS = {"compute":True}
    Nt_TS  = 201
    x_TS = [2.5, 5.0, 5.0, 7.5]
    y_TS = [5.0, 2.5, 7.5, 5.0]
    t_TS = np.linspace(0, exec_params["Tmax"], Nt_TS)
    TS["points"] = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/Nt_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = water_level_solution(x_TS[j], y_TS[j], t_TS, xo, yo, ho, sigma)
        TS_values.append([t_TS,w_TS])
    TS["measured_values"] = TS_values

    # Spatial Profile Config
    SP = {"compute":True}
    Np_SP  = 201
    t_SP  = [0.0, 25.0, 50.0, 100.0, 150.0, 200.0]
    x_SP  = np.linspace(xmin, xmax, Np_SP)
    y_SP  = yo + np.zeros(Np_SP)
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = water_level_solution(x_SP, y_SP, t_SP[j], xo, yo, ho, sigma)
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
