import numpy as np
from .. import Common
from .. import Default

def water_level_solution(x, y, t, xmax, ymax, ho, ro, w, a):
    """
    Computes the analytical time serie solution for the steady state flow
    """
    r = np.sqrt((x - xmax/2)**2 + (y - ymax/2)**2)
    A  = (a**2  - ro**2)/(a**2  + ro**2)

    B =  ho * ((r/a)**2 - 1.0)
    W =  ho*(np.sqrt(1 - A**2)/(1 - A*np.cos(w*t)) - 1.0 - (r/a)**2*((1 - A**2)/(1.0 - A*np.cos(w*t))**2 - 1.0)) 

    return np.maximum(B,W)

def get_raw_values(grid_dic, terminal_BCs):
    """
    DESCRIPTION:
    This analytical case attempts to study the the well-balanced property over a variable topography with dry zones. The objective of the 
    model is to test the ability to preserve stady state.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Thacker W., 'Some exact solution to the nonlinear shallow water equations'. Journal of Fluid Mechanics (107) 499" +
  		  "-508, 1981"] 

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":15.0, "g":Default.g(), "manning":0.0, "tol_dry":1E-08}

    ################################################################################
    # Parameters
    ################################################################################
    # Model Parameters
    a  = 1.00                # [m]
    ro = 0.80                # [m]
    ho = 0.10                # [m]
    g  = exec_params["g"]    # [m/s2]
    w = np.sqrt(8*ho*g/a**2) # [rad/s]

    # Discretization parameters
    xmin =  0.0   # [m]
    xmax =  4.0   # [m]
    ymin =  0.0   # [m]
    ymax =  4.0   # [m]
  
    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = ho
    grid_dic["u_mean"] = 0
    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    r  = np.sqrt((x - xmax/2.0)**2 + (y - ymax/2.0)**2)
    B  = ho * ((r/a)**2 - 1.0)
    B  = np.array([x, y, B]).T

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    A  = (a**2  - ro**2)/(a**2  + ro**2)
    W  = ho*(np.sqrt(1.0 - A**2)/(1.0 - A) - 1.0 - (r/a)**2*((1 - A**2)/(1.0 - A)**2 - 1.0)) 
    W  = np.array([x, y, W]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    # Movie Animation Step
    movie = {"compute":True, "dt_save":0.50}

    # inundation map, heat map and arrival times maps
    inundation = {"compute":False}
    heatmap    = {"compute":False}
    arrival_time = {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Config
    TS = {"compute":True}
    N_TS = 201
    x_TS = [2.5, 1.5, 1.5,  2.5, 2.0]
    y_TS = [2.5, 2.5, 1.5,  1.5, 2.0]
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"] = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = water_level_solution(x_TS[j], y_TS[j], t_TS, xmax, ymax, ho, ro, w, a)
        TS_values.append( (t_TS, w_TS) )
    TS["measured_values"] = TS_values

    # Spatial Profile Config
    SP = {"compute":True}
    N_SP  = 201
    t_SP  = [0.0, 0.5625, 1.125, 1.6875, 2.25, 2.8125, 3.375, 3.9375, 4.5, 9.00]
    x_SP  = np.linspace(xmin, xmax, N_SP)
    y_SP  = np.zeros(N_SP) + ymax/2.0
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = water_level_solution(x_SP, y_SP, t_SP[j], xmax, ymax, ho, ro, w, a)
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
