import numpy as np
from .. import Common
from .. import Default

# Convenient wrapper for the sech
sech = lambda x: 2.0*np.exp(-1.0*x)/(1.0 + np.exp(-2.0*x))

def water_level_solution(x, y, t, hmax, d, c, gamma, x0):
    """
    Computes the analytical solution for the propagation of solitary wave
    """
    w = hmax * sech(gamma*(x - x0 - c*t))**2

    return w

def get_raw_values(grid_dic, terminal_BCs):
    """
    This analytical case attempts to study the propagation of a solitary wave on a constant bathymetry. It is supposed to conserve 
    the water height during the whole initial_values. Loss of water height is not expected, but transformation of the profile due to
    nonlinear effects is expected.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Pierre Lubin and H. Lemonnier, 'Propagation of solitary waves in constant depths over horizontal', Journal of "    + 
                  "Multiphase Science and Technology (16) 239-250, 2004", "Yamazaki Y., Kowalik Z., and Cheung K., 'Depth-integrated " + 
 	   	  "non-hydrostatic model for wave breaking and run-up, International Journal for Numerical methods in fluids (61)"    + 
		  "473-497 , 2008"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":75.0, "g":Default.g(), "manning":0.0, "tol_dry":Default.tol_dry()}

    ################################################################################
    # MODEL PARAMETERS
    ################################################################################
    # Suggested Model Parameters
    d      = 25.00            # [m]
    g      = exec_params["g"] # [m/s2]
    hmax   = 1.250            # [m]
    x0     = 500.0            # [m]

    # Discretization parameters
    xmin   = 0.00   # [m]
    xmax   = 3000.0 # [m]
    ymin   = -5.0   # [m]
    ymax   =  5.0   # [m]

    # Physical parameters for the construction of the wave profile
    gamma = np.sqrt( 0.75 * hmax/d**3) # [1/m]
    c = np.sqrt(g*(hmax + d))          # [m/s]

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = d
    grid_dic["u_mean"] = c

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    B  = np.array([-d])

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    Z  = hmax*sech(gamma*(x - x0))**2
    W  = np.array([x, y, Z]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    U0 = np.sqrt(g/d) * Z * (1.00 - 0.25*Z/d)
    U0 = np.array([x, y, U0]).T
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    # Movie Animation Step
    movie = {"compute":True, "dt_save":10.0}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":False}
    heatmap      = {"compute":False}
    arrival_time = {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configuration
    N_TS = 201
    TS   = {"compute":True}
    x_TS = [500, 1000, 1500, 2000, 2500]
    y_TS = [0, 0, 0, 0, 0]
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"]  = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = water_level_solution(x_TS[j], y_TS[j], t_TS, hmax, d, c, gamma, x0)
        TS_values.append( (t_TS, w_TS) )
    TS["measured_values"] = TS_values

    # Spatial Profile Configuration
    N_SP = 201
    SP   = {"compute":True}
    t_SP = [0, 30, 60, 90, 120, 150, 180]
    x_SP = np.linspace(xmin, xmax, N_SP)
    y_SP = np.zeros(N_SP)
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = water_level_solution(x_SP, y_SP, t_SP[j], hmax, d, c, gamma, x0)
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
