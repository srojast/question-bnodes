import numpy as np
import sys
import os

import ArgumentHandler
import LonLatHandler
import Default
import Common
import SIPAT
import Mesh

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
################################################################################
def generate(options):
    """
    Generate test from SIPAT file.
    Comcot data format according to SIPAT format, discussed in SIPAT.py
    **Input**:
    **Output**:
    """

    # Arguments that are accepted for SIPAT (all others rejected)
    arguments = ["sipat", "to", "line", "id"]
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)

    # Arguments that have to be present
    arguments = ["sipat", "to"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)
    options["to"] = options["to"].strip()

    # Get the values
    if options["to"][-1] == "/":
        to_path = options["to"][:-1]
    else:
        to_path = options["to"]
    sipat_filepath = os.path.abspath(options["sipat"])

    # Check if SIPAT file exists
    if not ArgumentHandler.file_exists(sipat_filepath):
        return sys.exit(-1)

    # Check that line and id cannot be both selected
    valid_arguments = ["line", "id"]
    if not ArgumentHandler.at_most_one_argument(options, valid_arguments):
        return sys.exit(-1)

    # Avoid repetitions and sort results
    line_list = options["line"]
    line_list = sorted(list(set(line_list))) if (line_list is not None) else []
    sid_list = options["id"]
    sid_list = sorted(list(set(sid_list))) if (sid_list is not None) else []

    # Generate the mesh using the properties
    folders = SIPAT.generate_from_sipat_file(sipat_filepath, to_path, line_list, sid_list)

    # Print the usage for the benchmark, if only one folder was created.
    # Otherwise, most likely in batch and no info will be needed.
    if len(folders)==1:
        Common.print_usage(folders[0])

    return sys.exit(0)
