import numpy as np
import sys
import os

import LonLatHandler
import SBFLoader
import SBFSaver
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

'''
sbf_dict has the following keys:
 * "Mesh": array of dims [NPoints, 2]
 * "Bathymetry": arrays of dims [NPoints, 1] evaluated on Mesh.
 * "CoordinateSystem": string "geographical" or "cartesian"
 * "TimeSeriesBuoyCoordinates": (Optional) arrays of dims [NTS, 2].
 * "SpatialProfileBuoyCoordinates": (Optional) arrays of dims [NSP, 2].
 * "LinearizationPoint": (Optional) list [Lon, Lat] used for linearization
                          if converted to cartesian from geographical.
 * "MeshAdaptor": (Optional) dict with data pickled in mesh adapting process.
 '''

################################################################################
# README FOR SIMPLIFIED FORMAT.
# KEEP UPDATED THIS DOC IF FORMAT CHANGES
# READ BEFORE CHANGING ANY FUNCTION
################################################################################
def write_readme(to_path):
    readme = """
SBF: SIMPLIFIED BATHYMETRY FORMAT
=================================
SBF is a simplified bathymetry format that is easier to produce/mantain,
although it lacks the proper structure to be run by answer.
In particular, it does not have defined the triangular cells, it doesn't have
water surface or discharges. The simulation time, the manning coefficient, and
other physical parameters are not saved.
It might include time-series or spatial-profile buoys. The simplified model is
suited for tsunami scenarios, and the missing parameters will be filled when
converted to QAS format using the default values.

NUMBER FORMAT:
    Dot is used as decimal separator, as in 3.141592 or 4.2E+1

UNITS:
    The coordinates for the bathymetry and the buoys can be given in meters
    (relative to the center of the mesh) or in degrees (of longitude and latitude).
    The units are specificed in the Units.txt file.

SIGN CONVENTION:
    Bathymery: positive over water level, negative below water level.
    Position in meters:
        x: positive to the right.
        y: positive for upwards.
    Position in degrees:
        Longitude: positive for East.
        Latitude: positive for North.

The folder must have the following file structure:

REQUIRED FILES:

    ./Bathymetry.txt
        Plain text file with the Bathymetry at the mesh coordinates.
        Each line has a three values:
            (x, y, bathymetry) or (lon, lat, bathymetry)
        Units for Bathymetry are always meters.

    ./CoordinateSystem.txt
        Plant text file with the coordinate system used to express the coordinates
        of bathymetry and buoys.

        When producing hand-made files, create them in geographical
        coordinate system (Longitude and Latitude) and create a file
        CoordinateSystem.txt with the following single line:
        # Longitude [degrees], Latitude [degrees]

        When QUESTION creates files, the data will be in
        cartesian coordinate system, in meters,
        and the linearization technique and linearization point will be
        described in the file in the form of the following lines:
        # Cartesian Coordinate System
        # Using: projection from sphere into plane.
        # Linearization point (Longitude, Latitude): (Lon_value, Lat_value)

OPTIONAL FILES:

    ./TimeSeriesBuoyCoordinates.txt
        Plain text file with the coordinates of the Time Series Buoys.
        Each line has two values:
            (x, y, bathymetry) or (lon, lat, bathymetry)
        Longitude sign convention: positive for East.
        Latitude sign convention: positive for North.

    ./SpatialProfileBuoyCoordinates.txt
        Plain text file with the coordinates of the Time Series Buoys.
        Each line has two values:
            (x, y, bathymetry) or (lon, lat, bathymetry)
        Longitude sign convention: positive for East.
        Latitude sign convention: positive for North.

    ./MeshAdaptor.pkl
        Pickled file with required variables to relaunch the iterative
        process to adapt the mesh to bathymetry.

"""
    with open(os.path.join(to_path, "README.txt"), "w") as fw:
        fw.write(readme)
    return


################################################################################
# TESTS FOR SIMPLIFIED FILE FORMAT
################################################################################
def has_valid_format(fpath):
    """
    Check if provided path has the simplified format.
    """
    if not os.path.exists(fpath):
        print("Folder {0} does not exists.".format(fpath))
        return sys.exit(-1)
    required_files = ["Bathymetry.txt", "CoordinateSystem.txt"]
    in_simplified_format = all( os.path.exists(os.path.join(fpath, sfile)) for sfile in required_files)
    return in_simplified_format

################################################################################
# LOAD DATA IN SIMPLIFIED FORMAT
################################################################################
def load(fpath):
    """
    Returns the sbf dictionary with the data available in the provided folder.
    """
    if has_valid_format(fpath):
        return SBFLoader.load_as_sbf_dict(fpath)

################################################################################
# SAVE DATA IN SIMPLIFIED FORMAT
################################################################################
def save(to_path, sbf_dict, convert_to_cartesian=False):
    """
    Saves the sbf dictionary into sbf format on the provied folder.
    """
    if convert_to_cartesian:
        convert_dict_geographical_to_cartesian(sbf_dict)
    SBFSaver.save(to_path, sbf_dict)

################################################################################
# Convert data to mks if Meter2LatLon is provided
################################################################################
def convert_dict_geographical_to_cartesian(sbf_dict):
    """
    Converts the sbf dict data to meters.
    SBF dict has the keys:
        mesh, B, CoordinateSystem, LinearizationPoint
    And optionally the keys:
        TimeSeriesBuoyCoordinates, SpatialProfileBuoyCoordinates, MeshAdaptor.
    """
    if sbf_dict["CoordinateSystem"] == "cartesian":
        # Already in cartesian coordinate system
        return

    # Get the Linearization Point
    LP = sbf_dict["Mesh"].min(axis=0) + 0.5 * sbf_dict["Mesh"].ptp(axis=0)

    # Update the Coordinate System
    sbf_dict["CoordinateSystem"] = "cartesian"
    sbf_dict["LinearizationPoint"] = LP

    # Convert the mesh
    sbf_dict["Mesh"] = LonLatHandler.geographical_to_cartesian(sbf_dict["Mesh"], LP)
    # Convert the optional keys
    if "TimeSeriesBuoyCoordinates" in sbf_dict:
        sbf_dict["TimeSeriesBuoyCoordinates"] = LonLatHandler.geographical_to_cartesian(sbf_dict["TimeSeriesBuoyCoordinates"], LP)
    if "SpatialProfileBuoyCoordinates" in sbf_dict:
        sbf_dict["SpatialProfileBuoyCoordinates"] = LonLatHandler.geographical_to_cartesian(sbf_dict["SpatialProfileBuoyCoordinates"], LP)

    # Mesh Adaptor never needs to update its variables.
    return
