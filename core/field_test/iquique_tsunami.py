import numpy as np
import os

from .. import RemoteFiles
from .. import LonLatHandler
from .. import Common 
from .. import Default

#from IPython import embed as ip

def time_serie_values(j):
    """
    Reads the analytical solution for the spatial profile
    """
    Dir = ".".join(__file__.split(".")[:-1]) + ".txt"
    Vals = np.loadtxt(Dir,dtype = float)

    t = Vals[:,0]
    w = Vals[:,j+1]

    return t, w

def get_raw_values(grid, terminal_BCs, folder_path):
    """
    This field case attempts to assess the evolution of the Chlilean tsunami that took place on April, the 3rd, 2014. several time 
    series in Iquique and Arica were recorded. Moreover, there are information of  six-nested grid in order to increase the resolution 
    specially close to the coast of Iquique and Arica. This case, is a fine example that not only tests a large scale event onto a complex 
    geometry, but also employs five meshes with different resolution in certains areas.  
    """
    #==========================================================================================================================================
    # References
    #==========================================================================================================================================
    references = []

    #==========================================================================================================================================
    # Suggested parameter usage
    #==========================================================================================================================================
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params = {"Tmax":18000, "g":Default.g(), "manning":0.001} 

    #==========================================================================================================================================
    # Mesh
    #==========================================================================================================================================
    Mesh = np.loadtxt(os.path.join(folder_path,"Mesh.txt"))

    #==========================================================================================================================================
    # Bathymetry: (B)
    #==========================================================================================================================================
    B = np.loadtxt(os.path.join(folder_path,"Bathymetry.txt")).T
    B = np.array([Mesh[:,0],Mesh[:,1],B]).T

    #==========================================================================================================================================
    # Water Level Surface: (W)
    #==========================================================================================================================================
    W = np.loadtxt(os.path.join(folder_path,"WaterLevel.txt")).T
    W = np.array([Mesh[:,0],Mesh[:,1],W]).T

    #==========================================================================================================================================
    # Flux Velocities: (U0,V0)
    #==========================================================================================================================================
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    #==========================================================================================================================================
    # Change of coordinates: (Lat,lon) -> (meters,meters)
    #==========================================================================================================================================
    # Discretization parameters
    Lon_min = np.min(Mesh[:,0])  
    Lon_max = np.max(Mesh[:,0])
    Lat_min = np.min(Mesh[:,1])
    Lat_max = np.max(Mesh[:,1])

    Lon0, Lat0 = .5*(Lon_min + Lon_max), .5*(Lat_min + Lat_max)

    #Transformation Table:
    m2deg = LonLatHandler.generate_meter2lonlat_table(Mesh[:,0].T, Mesh[:,1].T, Lon0, Lat0)

    #==========================================================================================================================================
    # Required Output
    #==========================================================================================================================================
    # Movie Animation Step
    output = {}
    output["movie_config"] =  {"dt_save":200.0, "compute_floading_and_arrival_time":True}

    # Time Series Config
    qp = np.array([[-70.404441,-23.653133],[-70.1478305555,-20.2045777778],[-70.3232305,-18.475800],
		   [-70.450830,-23.097500],[-70.4687280000,-25.0089820000],[-70.1980290,-20.803213],
		   [-70.215982,-19.596890],[-70.2116530000,-22.0937570000]])

    x_TS = qp[:,0]
    y_TS = qp[:,1]
    output["TS_config"] = [x_TS, y_TS]

    # Time Series Known Values
    output["TS_values"] = []
    for j in range(len(x_TS)):
	t_prof, w_prof = time_serie_values(j)
        output["TS_values"].append(t_prof)
        output["TS_values"].append(w_prof)

    # Spatial Profile Config
    output["SP_values"] = [ ]

    # Spatial Profile Config
    output["SP_config"] = [ ]

    #==========================================================================================================================================
    # Return dic with raw test values
    #==========================================================================================================================================
    return {"Mesh"          : Mesh, 
	    "B"             : B, 
	    "W"             : W, 
	    "U0"            : U0, 
	    "V0"            : V0, 
            "Meter2Degrees" : m2deg,
	    "output"        : output, 
	    "BC_list"       : test_BC, 
	    "references"    : references, 
	    "params"        : exec_params}

