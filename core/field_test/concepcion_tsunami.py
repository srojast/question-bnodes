import numpy as np
import os

from .. import RemoteFiles
from .. import LonLatHandler
from .. import Common 
from .. import Default

#from IPython import embed as ip
#from pdb import set_trace as st

def time_serie_values(j):
    """
    Reads the analytical solution for the spatial profile
    """
    Dir = ".".join(__file__.split(".")[:-1]) + ".txt"
    Vals = np.loadtxt(Dir,dtype = float)

    t = Vals[:,0]
    w = Vals[:,j+1]

    return t, w

def get_raw_values(grid, terminal_BCs, folder_path):
    """
    Chilean tsunami that took place on February, the 27th, 2010. Only two time series in Valparaiso and Arauco were recorded. Some 
    information is missing in one of the bouys. This case provides a nice example with large scale event and complex geometry 
    (multi-resolution mesh)
    """
    #==========================================================================================================================================
    # References
    #==========================================================================================================================================
    references = []

    #==========================================================================================================================================
    # Suggested parameter usage
    #==========================================================================================================================================
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params = {"Tmax":60300, "g":Default.g(), "manning":0.001} 

    #==========================================================================================================================================
    # Mesh
    #==========================================================================================================================================
    Mesh = np.loadtxt(os.path.join(folder_path,"Mesh.txt"))

    #==========================================================================================================================================
    # Bathymetry: (B)
    #==========================================================================================================================================
    B = np.loadtxt(os.path.join(folder_path,"Bathymetry.txt"))
    B = np.array([Mesh[:,0],Mesh[:,1],B]).T

    #==========================================================================================================================================
    # Water Level Surface: (W)
    #==========================================================================================================================================
    W = np.loadtxt(os.path.join(folder_path,"WaterLevel.txt"))
    W = np.array([Mesh[:,0],Mesh[:,1],W]).T

    #==========================================================================================================================================
    # Flux Velocities: (U0,V0)
    #==========================================================================================================================================
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    #==========================================================================================================================================
    # Change of coordinates: (Lat,lon) -> (meters,meters)
    #==========================================================================================================================================
    # Discretization parameters
    Lon_min = np.min(Mesh[:,0])  
    Lon_max = np.max(Mesh[:,0])
    Lat_min = np.min(Mesh[:,1])
    Lat_max = np.max(Mesh[:,1])

    Lon0, Lat0 = .5*(Lon_min + Lon_max), .5*(Lat_min + Lat_max)

    #Transformation Table:
    m2deg = LonLatHandler.generate_meter2lonlat_table(Mesh[:,0].T, Mesh[:,1].T, Lon0, Lat0)

    #==========================================================================================================================================
    # Required Output
    #==========================================================================================================================================
    # Movie Animation Step
    output = {}
    output["movie_config"] =  {"dt_save":300.0, "compute_floading_and_arrival_time":True}

    # Time Series Config
    qp = np.array([[-73.1911,-36.7941],[-71.575,-33.016]])
    x_TS = qp[:,0]
    y_TS = qp[:,1]
    output["TS_config"] = [x_TS, y_TS]

    # Time Series Known Values
    output["TS_values"] = []
    for j in range(len(x_TS)):
	t_prof, w_prof = time_serie_values(j)
        output["TS_values"].append(t_prof)
        output["TS_values"].append(w_prof)

    # Spatial Profile Config
    output["SP_values"] = [ ]

    # Spatial Profile Config
    output["SP_config"] = [ ]

    #==========================================================================================================================================
    # Return dic with raw test values
    #==========================================================================================================================================
    return {"Mesh"          : Mesh, 
	    "B"             : B, 
	    "W"             : W, 
	    "U0"            : U0, 
	    "V0"            : V0, 
            "Meter2Degrees" : m2deg,
	    "output"        : output, 
	    "BC_list"       : test_BC, 
	    "references"    : references, 
	    "params"        : exec_params}

