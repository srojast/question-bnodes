import numpy as np
import pickle
import sys
import os

import SBFFormat as SBF
import QASFormat as QAS
import ArgumentHandler
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
################################################################################
def generate(options):
    """
    Generate test from Simplified formatted folder.
    """

    # Must have argument from_simplified. All other are optional combinations.
    arguments = ["from_sbf"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)
    options["from_sbf"] = options["from_sbf"].strip()

    from_path = options["from_sbf"]

    # Check for format of provided folder
    if not SBF.has_valid_format(from_path):
        print("Provided folder not in Simplified format")
        sys.exit(-1)

    # Can only have to or to_simplified argument, not both
    arguments = ["to_sbf", "to"]
    if not ArgumentHandler.only_one_argument(options, arguments):
        return sys.exit(-1)

    ##################################################################
    # Action associated to flags from_sbf and to : Simplest conversion
    ##################################################################
    if options["to"] is not None:
        options["to"] = options["to"].strip()
        # valid arguments
        arguments = ["from_sbf", "to", "Bnodes"]
        if not ArgumentHandler.accepted_arguments(options, arguments):
            return sys.exit(-1)

        # Load simplified format
        sbf_dict = SBF.load(from_path)

        # Try to load the pickle, if any
        pickle_filepath_from = os.path.join(from_path, "mesh_adapt.pkl")
        if os.path.exists(pickle_filepath_from):
            with open(pickle_filepath_from, "r") as pickle_reader:
                pickle_dict = pickle.load(pickle_reader)
                domain_points = pickle_dict["domain_points"]
                if "remove_polygon" in pickle_dict:
                    remove_polygon = pickle_dict["remove_polygon"]
                # LEGACY, TO BE REMOVED!!!
                if "exclude_bbox_points" in pickle_dict:
                    remove_polygon = pickle_dict["exclude_bbox_points"]
        else:
            domain_points = None
            remove_polygon = None

        # Convert to cartesian coordinates before saving
        SBF.convert_dict_geographical_to_cartesian(sbf_dict)

        # Get the values
        to_path = options["to"]
        saveBnodes = options["Bnodes"]

        # Save in QAS format
        if "TimeSeriesBuoyCoordinates" in sbf_dict:
            TS = sbf_dict["TimeSeriesBuoyCoordinates"]
        else:
            TS = np.array([])
        if "SpatialProfileBuoyCoordinates" in sbf_dict:
            SP = sbf_dict["SpatialProfileBuoyCoordinates"]
        else:
            SP = np.array([])
        QAS.save_from_sbf_dict(to_path, sbf_dict, saveBnodes, domain_points, remove_polygon)

        # Print the usage for the benchmark
        Common.print_usage(to_path)

        return sys.exit(0)

    return sys.exit(-1)
