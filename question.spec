# -*- mode: python -*-

a = Analysis(['question.py'],
             pathex=['./'],
             hiddenimports=['scipy.integrate'],
             hookspath=None,
             runtime_hooks=None,
             excludes=None)
pyz = PYZ(a.pure)

a.datas += [ ('core/field_test/concepcion_tsunami.txt', 'core/field_test/concepcion_tsunami.txt', 'DATA'), 
             ('core/field_test/iquique_tsunami.txt', 'core/field_test/iquique_tsunami.txt', 'DATA'),
             ('core/field_test/run_up_onto_a_complex_3d_beach.txt', 'core/field_test/run_up_onto_a_complex_3d_beach.txt', 'DATA'),
             ('core/lab_test/solitary_wave_on_a_composite_beach.txt','./core/lab_test/solitary_wave_on_a_composite_beach.txt','DATA'),
             ('core/lab_test/solitary_wave_on_a_conical_island.txt','./core/lab_test/solitary_wave_on_a_conical_island.txt','DATA'),
             ('core/lab_test/solitary_wave_on_a_simple_beach.txt','./core/lab_test/solitary_wave_on_a_simple_beach.txt','DATA')]

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='question',
          debug=False,
          strip=None,
          upx=True,
          console=True )

