import os

SHOULD_PASS = True
SHOULD_FAIL = False

def get_testing_enviroment(clean=False):
    testing_path = os.path.dirname(__file__)
    testing_env = os.path.join(testing_path, "tmp") 
    if clean:
        os.system("rm -rf {0}".format(testing_env))
    print testing_env
    return testing_env

def fancy_title(string):
    line = "\n"+"#"*80 + "\n"
    print(line + string + line)
    return
    
def print_in_color(string, color):
    """
    prints in colors
    """
    if color=="red":
        print('\033[93m' + string + '\033[0m')
    elif color=="green":
        print('\033[92m' + string + '\033[0m')
    else:
        print(string)
    return

def system(order, should_pass=SHOULD_PASS, redirect_to_null=True):
    """
    Prints and executes a system order
    """
    print(order)
    if redirect_to_null:
        redirected_order = order + " > /dev/null 2>&1"
        status = os.system(redirected_order)
    else:
        status = os.system(order)
    correctly_passed = (should_pass) and (status==0)
    correctly_failed = (not should_pass) and (status!=0)    
    if should_pass:
        if status==0:
            print_in_color("\tOK: passed when expected.","green")
        else:
            print_in_color("\tError: should have passed (and it failed).", "red")            
    else:
        if status!=0:
            print_in_color("\tOK: failed when expected.","green")
        else:
            print_in_color("\tError: Should have failed (and it passed).", "red")            
    return
