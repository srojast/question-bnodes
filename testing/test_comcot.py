from ExecutionTesting import system, fancy_title, get_testing_enviroment
from ExecutionTesting import SHOULD_PASS, SHOULD_FAIL
import os

################################################################################
# Clean before making the tests
################################################################################
env = get_testing_enviroment(clean=True)
comcot_data_path = "data"

################################################################################
# Tests that must fail misserably
################################################################################
fancy_title("Tests that should fail (mixing flags & inexisting flags)")

# Must use a valid comcot folder
comcot_folder = os.path.join(comcot_data_path, "non_existing")
to_folder = os.path.join(env, "wrong")
system("python question.py --comcot {0} --to {1}".format(comcot_folder, to_folder), SHOULD_FAIL)

# Do not use inexisting flags
comcot_folder = os.path.join(comcot_data_path, "comcot_small_example")
to_folder = os.path.join(env, "wrong")
system("python question.py --comcot {0} --to {1} --not_existing_flag".format(comcot_folder, to_folder), SHOULD_FAIL)

# Do not mix valid flags
comcot_folder = os.path.join(comcot_data_path, "comcot_small_example")
to_folder = os.path.join(env, "wrong")
system("python question.py --comcot {0} --to {1} --benchmark 22".format(comcot_folder, to_folder), SHOULD_FAIL)

################################################################################
# Simple Tests that must work
################################################################################
# Do a small comcot file conversion, check IC with sweet
comcot_folder = os.path.join(comcot_data_path, "comcot_small_example")
to_folder = os.path.join(env, "comcot_to_question_small_example")
system("python question.py --comcot {0} --to {1}".format(comcot_folder, to_folder), SHOULD_PASS)
system("sweet -ic {0}".format(to_folder), SHOULD_PASS)
system("sweet -ic {0} --degrees".format(to_folder), SHOULD_PASS)


"""
# Do the big comcot file conversion
comcot_folder = os.path.join(comcot_data_path, "comcot_big_example")
to_folder = os.path.join(env, "comcot_to_question_big_example")
system("python question.py --comcot {0} --to {1}".format(comcot_folder, to_folder), SHOULD_PASS)
"""
