import numpy.random as rnd
import numpy as np
import scipy.interpolate as interp
npoints=100000
grid=False
niters=1000
n=0
x=rnd.uniform(-10,10,npoints)
y=rnd.uniform(-10,10,npoints)
values=(2*x+y**2)
xi=np.linspace(-10,10,2*npoints)
yi=np.linspace(-10,10,2*npoints)


interpolator=interp.LinearNDInterpolator((x, y), values)


while n<niters:
   n=n+1
   if grid:
      zi=interp.griddata((x, y), values, (xi, yi), method='linear')
   else:
      zi=interpolator((xi, yi))
   print zi.max()
