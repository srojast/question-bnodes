from ExecutionTesting import system, fancy_title, get_testing_enviroment
from ExecutionTesting import SHOULD_PASS, SHOULD_FAIL
import os

################################################################################
# Clean before making the tests
################################################################################
env = get_testing_enviroment(clean=True)

################################################################################
# Create folder to be modified
################################################################################
fancy_title("Base folder that will be modified")
N = 22
from_folder = os.path.join(env, "cwl")
system("python question.py --benchmark {0} --to {1}".format(N, from_folder), SHOULD_PASS)

################################################################################
################################################################################
# TESTS FOR FLAT
################################################################################
################################################################################

################################################################################
fancy_title("Test that should fail for --flat")
################################################################################

# from_folder must exist
system("python question.py --from inexistent_folder --flat 0 0 0 --to another_folder", SHOULD_FAIL)

# Shouldn't mix flags, exit cleanly
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "wrong")
system("python question.py --from {0} --flat 0 0 0 --benchmark 21 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)

# Must have --to
from_folder = os.path.join(env, "wrong")
system("python question.py --from {0} --flat 0 0 0".format(from_folder), SHOULD_FAIL)

# Must have --from
to_folder = os.path.join(env, "wrong")
system("python question.py --flat 0 0 0 --to {0}".format(to_folder), SHOULD_FAIL)

# Incorrect number of parameters
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "wrong")
system("python question.py --from {0} --bell 0 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)
system("python question.py --from {0} --bell 0 0 0 0 0 0 0 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)

################################################################################
fancy_title("Test that should work for --flat")
################################################################################

# Overwrite folder
same_folder = os.path.join(env, "cwl")
system("python question.py --from {0}  --flat 0.4 0.0 0.0 --to {0}".format(same_folder), SHOULD_PASS)

# Positive value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_flat_positive")
system("python question.py --from {0} --flat 0.1 1.0 0.5 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# value 0
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_flat_zero")
system("python question.py --from {0} --flat 0 0 0 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# negative value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_flat_neg")
system("python question.py --from {0} --flat -0.1 -0.2 -1.0 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# very negative value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_flat_very_neg")
system("python question.py --from {0} --flat -1000.0 -0.2 -1.0 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# Standard run
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_falt_ok")
system("python question.py --from {0}  --flat 0.2 1.0 -0.1 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

################################################################################
################################################################################
# TESTS FOR BELL
################################################################################
################################################################################

################################################################################
fancy_title("Test that should fail for --bell")
################################################################################

# from_folder must exist
system("python question.py  --from inexistent_folder --bell 0 0 1 2 --to another_folder" , SHOULD_FAIL)

# Shouldn't mix flags
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "wrong")
system("python question.py  --from {0} --bell 0 0 1 2 --benchmark 21 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)

# Must have --to
from_folder = os.path.join(env, "cwl")
system("python question.py --from {0} --bell 0 0 1 2".format(from_folder), SHOULD_FAIL)

# Must have --from
to_folder = os.path.join(env, "wrong")
system("python question.py --bell 0 0 1 2 --to {0}".format(to_folder), SHOULD_FAIL)

# Incorrect number of parameters
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "wrong")
system("python question.py --from {0} --bell 0 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)
system("python question.py --from {0} --bell 0 0 0 0 0 0 0 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)


################################################################################
fancy_title("Test that should work for --bell")
################################################################################
# Same folder
same_folder = os.path.join(env, "cwl")
system("python question.py  --from {0} --bell 8 2 1 2 --to {0}".format(same_folder), SHOULD_PASS)

# Positive value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_bell")
system("python question.py  --from {0} --bell 10 2 1 2 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# value 0 
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_bell_zero")
system("python question.py  --from {0} --bell 0 0 0 0 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# negative value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_bell_neg")
system("python question.py  --from {0} --bell -10 -2 -1 2 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# very negative value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_bell_very_neg")
system("python question.py  --from {0} --bell -10 0 -100 2 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# Standard run
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_bell")
system("python question.py  --from {0} --bell -8 -2 1 2 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

################################################################################
################################################################################
# TESTS FOR WAVE
################################################################################
################################################################################

################################################################################
fancy_title("Test that should fail for --wave")
################################################################################

# from_folder must exist
system("python question.py  --from inexistent_folder --wave 0 0 1 2 45 --to another_folder", SHOULD_FAIL)

# Shouldn't mix flags
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_wave")
system("python question.py  --from {0} --wave 0 0 1 2 45 --benchmark 21 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)

# Must have --to
from_folder = os.path.join(env, "cwl")
system("python question.py --from {0} --wave 0 0 1 2 45".format(from_folder), SHOULD_FAIL)

# Must have --from
to_folder = os.path.join(env, "wrong")
system("python question.py --wave 0 0 1 2 45 --to {0}".format(to_folder), SHOULD_FAIL)

# Incorrect number of parameters
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "wrong")
system("python question.py --from {0} --wave 0 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)
system("python question.py --from {0} --wave 0 0 0 0 0 0 0 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)

################################################################################
fancy_title("Tests that should fail for --wave")
################################################################################

# Same folders
same_folder = os.path.join(env, "cwl")
system("python question.py  --from {0} --wave 0 0 1 2 45 --to {0}".format(same_folder), SHOULD_PASS)

# Positive value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_wave_positive")
system("python question.py  --from {0} --wave 8 2 1 2 45 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# value 0 
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_wave_zero")
system("python question.py  --from {0} --wave 0 0 0 0 0 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# negative value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_wave_negative")
to_folder = "tmp/cwl_wave_neg"
system("python question.py  --from {0} --wave -8 2 -1 2 -45 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# very negative value
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_wave_very_negative")
to_folder = "tmp/cwl_wave_neg"
system("python question.py  --from {0} --wave -8 0 -2 1000 -45 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

# Standard run
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "cwl_wave")
to_folder = "tmp/cwl_wave_ok"
system("python question.py  --from {0} --wave 0 0 1 2 45 --to {1}".format(from_folder, to_folder), SHOULD_PASS)

################################################################################
# Complex failing tests for --flat, --wave and --bell
################################################################################
fancy_title("Complex tests that should fail")

# Shouldn't mix flags, exit cleanly
from_folder = os.path.join(env, "cwl")
to_folder = os.path.join(env, "wrong")
system("python question.py --from {0} --flat 0 0 0 --wave 0 0 1 2 45 --to {1}".format(from_folder, to_folder), SHOULD_FAIL)
